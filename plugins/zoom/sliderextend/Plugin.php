<?php namespace Zoom\SliderExtend;

use System\Classes\PluginBase;
use Illuminate\Support\Facades\Event;
use Grape\Slider\Models\SliderItem;
use Grape\Slider\Controllers\Sliders;

class Plugin extends PluginBase
{
    public $require = ['Grape.Slider'];

    public function boot()
    {
        $fields = [ 'layer_small', 'layer_medium', 'layer_large', 'layer_xlarge'];

        SliderItem::extend(function ($model) use ($fields) {
            foreach ($fields as $field) {
                $model->attachOne[$field] = ['System\Models\File', 'delete' => true];
            }
        });


        // Extend all backend form usage
        Event::listen('backend.form.extendFields', function ($widget) use ($fields) {

            if (!$widget->getController() instanceof Sliders) {
                return;
            }

            if (!$widget->model instanceof SliderItem) {
                return;
            }
            foreach ($fields as $field) {
                $widget->addFields([
                    $field => [
                        'label'         => 'zoom.sliderextend::lang.form.'.$field,
                        'mode'          => 'image',
                        'imageWidth'    => '200',
                        'imageHeight'   => '200',
                        'fileTypes'     => 'jpg,png,jpeg',
                        'useCaption'    => true,
                        'thumbOptions'  => [
                            'mode' => 'crop',
                            'extension' => 'png',
                        ],
                        'span'          => 'full',
                        'context'       => ['update', 'preview'],
                        'type'          => 'fileupload',
                        'tab'           => 'grape.blog::lang.tabs.poster'
                    ]
                ]);
            }

            // Remove a Surname field
            $widget->removeField('content');
        });
    }
}
