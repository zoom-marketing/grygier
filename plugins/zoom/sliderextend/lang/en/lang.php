<?php return [
    'plugin' => [
        'name' => 'SliderExtend',
        'description' => 'Rozszerza Slider o pole image do warstwy'
    ],
    'form' => [
        'layer_small' => 'Warstaw (max: 639px)',
        'layer_medium' => 'Warstaw (max: 959px)',
        'layer_large' => 'Warstaw (max: 1199px)',
        'layer_xlarge' => 'Warstaw (min: 1200px)',
    ]
];
