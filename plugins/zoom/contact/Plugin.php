<?php namespace Zoom\Contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Zoom\Contact\Components\Contact' => 'contact',
        ];
    }

    public function registerSettings()
    {
    }
}
