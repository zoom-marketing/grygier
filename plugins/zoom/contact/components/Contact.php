<?php
namespace Zoom\Contact\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;

class Contact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('zoom.contact::lang.component.documents.name'),
            'description' => trans('zoom.contact::lang.component.documents.description'),
        ];
    }
    public function onSubmit()
    {
//        dump(Input::all());
        var_dump($_FILES);
    }
}
