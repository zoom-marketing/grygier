<?php namespace Grape\Widgets\Classes\Traits;

use Illuminate\Support\Facades\Event;

trait WidgetsExtendPluginTrait
{
    protected function extendMenu($menuItem = null)
    {
        // Extend the navigation
        if (is_array($menuItem)) {
            Event::listen('backend.menu.extendItems', function ($manager) use ($menuItem) {
                $manager->addSideMenuItems('Grape.Widgets', 'widgets-main-menu', $menuItem);
            });
        }
    }
}
