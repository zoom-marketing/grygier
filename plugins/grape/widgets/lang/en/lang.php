<?php return [
    'plugin' => [
        'name' => 'Widgets',
        'description' => 'Widgets management.',
    ],
    'menu' => [
        'widgets' => 'Widgets',
    ],
    'global' => [
        'go' => 'more'
    ]
];
