<?php return [
    'plugin' => [
        'name' => 'Widgets',
        'description' => 'Widgets management.',
    ],
    'menu' => [
        'widgets' => 'Widgety',
    ],
    'global' => [
        'go' => 'więcej'
    ]
];
