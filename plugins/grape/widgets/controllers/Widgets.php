<?php namespace Grape\Widgets\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;

class Widgets extends Controller
{
    public $implement = [    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Grape.Widgets', 'widgets-main-menu');
        $this->pageTitle = trans('grape.widgets::lang.plugin.name');
        $this->addCss("/plugins/grape/widgets/assets/css/style.css", "1.0.0");
    }

    public function index()
    {
        $this->vars['menuItems'] = BackendMenu::listSideMenuItems();
    }
}
