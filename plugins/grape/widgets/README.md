# Grape.Widgets
Plugin służy do utworzenia zakładki menu gdzie można zarządzać mniejszymi pluginami.
##Instalacja

###Plugin

    use Grape\Widgets\Classes\Traits\WidgetsExtendPluginTrait;

    class Plugin extends PluginBase
    {

        use WidgetsExtendPluginTrait;

        public $require = ['Grape.Widgets'];

        public function boot()
        {
            $menuItem = [
                'grape.pluginA' => [
                    'label'       => trans('grape.pluginA::lang.menu.menuA'),
                    'icon'        => 'icon-th-large',
                    'code'        => 'side-menu-item',
                    'owner'       => 'Grape.Widgets',
                    'url'         => Backend::url('grape/pluginA/controllerA'),
                ],
            ];

            $this->extendMenu($menuItem);
        }
    }

###Controller

    class ControllerA extends Controller
    {
        public function __construct()
        {
            parent::__construct();
            BackendMenu::setContext('Grape.Widgets', 'widgets-main-menu');
        }
    }
