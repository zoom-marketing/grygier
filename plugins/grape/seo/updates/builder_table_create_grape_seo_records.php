<?php namespace Grape\Seo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeSeoRecords extends Migration
{
    public function up()
    {
        Schema::create('grape_seo_records', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('seoable_id')->nullable();
            $table->string('seoable_type', 255)->nullable();
            $table->boolean('is_active')->nullable()->default(1);
            $table->string('title', 64)->nullable();
            $table->string('description', 255)->nullable();
            $table->string('keywords', 255)->nullable();
            $table->string('robots', 128)->nullable();
            $table->string('canonical', 255)->nullable();
            $table->text('additional_code')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_seo_records');
    }
}
