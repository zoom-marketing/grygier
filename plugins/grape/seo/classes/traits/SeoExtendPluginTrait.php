<?php namespace Grape\Seo\Classes\Traits;

use Yaml;
use File;
use Grape\Seo\Models\Seo;

trait SeoExtendPluginTrait
{
    protected $extend = null;

    protected function extendModel()
    {
        if (!$this->extend) {
            return;
        }
        foreach ($this->extend as $extend) {
            $modelClass = $extend['model'];
            $modelClass::extend(function ($model) {
                $model->implement[] = 'Grape.Seo.Behaviors.SeoModel';
            });
        }
    }

    protected function extendForm()
    {
        if (!$this->extend) {
            return;
        }

        foreach ($this->extend as $extend) {
            $modelClass = $extend['model'];
            $controllerClass = $extend['controller'];
            $controllerClass::extendFormFields(function ($form, $model, $context) use ($modelClass) {
                if (!$model instanceof $modelClass) {
                    return;
                }
                if (!$model->exists) {
                    return;
                }
                Seo::getFromModel($model);
                $configFile = plugins_path('grape/seo/config/seo_fields.yaml');
                $config = Yaml::parse(File::get($configFile));
                $form->addTabFields($config);
            });
        }
    }
}
