<?php namespace Grape\Seo\Models;

use Model;

/**
 * Model
 */
class Seo extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_seo_records';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title', 'description', 'keywords'];

    /**
     * @var array Validation rules
     */
    public $rules = [

        'title' => 'nullable|string|min:2|max:64',
        'description' => 'nullable|string|min:2|max:64',
        'keywords' => 'nullable|string|min:2|max:255',
        'robots' => 'nullable|string|min:2|max:128',
        'canonical' => 'nullable|url',
        'additional_code' => 'nullable',
    ];

    protected $fillable = ['is_active', 'title', 'description', 'keywords', 'robots', 'canonical', 'additional_code'];

    public $morphTo = [
        'seoable' => []
    ];

    public static function getFromModel($model)
    {
        //TODO: może trzeba sprawdić czy dany model ma zadeklaowaną relację.
        if ($model->seo) {
            return $model->seo;
        }
        $seo = new static;
        $seo->seoable()->associate($model)->save();
    }
}
