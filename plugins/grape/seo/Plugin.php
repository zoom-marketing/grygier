<?php namespace Grape\Seo;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Grape\Seo\Components\Seo' => 'seo',
        ];
    }
}
