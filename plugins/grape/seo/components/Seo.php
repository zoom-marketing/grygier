<?php
namespace Grape\Seo\Components;

use Cms\Classes\ComponentBase;
use Grape\Seo\Models\Seo as SeoModel;

class Seo extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.seo::lang.component.posts.name'),
            'description' => trans('grape.seo::lang.component.posts.description'),
        ];
    }

    public function seo($record)
    {
        $content = '';
        if ($record instanceof SeoModel) {
            $content = $this->renderPartial('@default.htm', ['seo' => $record]);
        }
        return $content;
    }
}
