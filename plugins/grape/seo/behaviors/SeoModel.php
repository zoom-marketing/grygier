<?php namespace Grape\Seo\Behaviors;

use System\Classes\ModelBehavior;

class SeoModel extends ModelBehavior
{
    public function __construct($model)
    {
        parent::__construct($model);
        $model->morphOne['seo'] = ['Grape\Seo\Models\Seo', 'name' => 'seoable'];
    }
}
