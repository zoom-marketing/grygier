#Grape.SEO

## Integracja

W celu integracji z dowolnym Modelem należy stworzyć następny plugin w celu rozszeżenia Modelu. Przykład:

    use System\Classes\PluginBase;
    use Grape\Seo\Classes\Traits\SeoExtendPluginTrait;
    use Grape\PluginA\Models\ModelA;
    use Grape\PluginA\Controllers\ControllerA;

    class Plugin extends PluginBase
    {
        use SeoExtendPluginTrait;

        public $require = ['Grape.PluginA', 'Grape.Seo'];

        public function boot()
        {
            $this->model = ModelA::class;
            $this->controller = ControllerA::class;
            $this->extendModel();
            $this->extendForm();
        }
    }

## Użycie

Po rozszerzeniu mam dostęp do relacji z seo:

    $modelA->seo
