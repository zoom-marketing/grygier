<?php return [
    'plugin' => [
        'name' => 'SEO',
        'description' => 'SEO plugin.',
    ],
    'form' => [
        'id' => 'ID',
        'is_active' => 'Active',
        'title' => 'Title',
        'description' => 'Description',
        'keywords' => 'Keywords',
        'robots' => 'Robots',
        'canonical' => 'Canonical URL',
        'additional_code' => 'Additional Code',
        'seoable_type' => 'Model',
        'seoable_id' => 'Model ID',
    ],
    'tabs' => [
        'seo' => 'SEO',
    ],
    'menu' => [
        'main' => 'SEO',
    ],
    'permission' => [
        'access' => 'Full Access'
    ]
];
