<?php namespace Grape\Seo\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Seo extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    protected $requiredPermissions = [
        'grape.seo.access'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Grape.Seo', 'grape-seo-main');
    }
}
