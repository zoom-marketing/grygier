<?php namespace Grape\PagesSeoConnector;

use System\Classes\PluginBase;
use Grape\Pages\Models\Page;
use Grape\Pages\Controllers\Pages;
use Grape\Seo\Classes\Traits\SeoExtendPluginTrait;

class Plugin extends PluginBase
{
    use SeoExtendPluginTrait;

    public $require = ['Grape.Pages', 'Grape.Seo'];

    public function boot()
    {
        $this->extend = [
            [
                'model' => Page::class,
                'controller' => Pages::class
            ]
        ];
        $this->extendModel();
        $this->extendForm();
    }
}
