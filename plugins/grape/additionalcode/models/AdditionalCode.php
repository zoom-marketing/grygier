<?php namespace Grape\AdditionalCode\Models;

use Model;

class AdditionalCode extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'grape_additionalcode';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

    public $attachOne = [
        'app_icon' => 'System\Models\File'
    ];

}
