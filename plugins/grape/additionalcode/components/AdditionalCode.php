<?php
namespace Grape\AdditionalCode\Components;

use Grape\AdditionalCode\Models\AdditionalCode as AdditionalCodeSettings;

class AdditionalCode extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.additionalcode::lang.component.additionalcode.name'),
            'description' => trans('grape.additionalcode::lang.component.additionalcode.description'),
        ];
    }

    public function onRun()
    {
        $this->page['appIcon']     = AdditionalCodeSettings::instance()->app_icon;
        $this->page['appColor']    = AdditionalCodeSettings::instance()->app_color;
        $this->page['scripts']     = AdditionalCodeSettings::instance()->scripts;
        $this->page['styles']      = AdditionalCodeSettings::instance()->styles;
        $this->page['headAfter']   = AdditionalCodeSettings::instance()->head_after;
        $this->page['bodyBefore']  = AdditionalCodeSettings::instance()->body_before;
        $this->page['bodyAfter']   = AdditionalCodeSettings::instance()->body_after;
    }
}
