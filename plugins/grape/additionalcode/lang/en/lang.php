<?php return [
    'plugin' => [
        'name' => 'Additional Code',
        'description' => 'HTML, CSS & JS',
    ],
    'form' => [
        'app_icon' => 'App Icon',
        'scripts' => 'Script',
        'styles' => 'Style',
        'head_after' => 'HEAD::after',
        'body_before' => 'BODY::before',
        'body_after' => 'BODY::after',
        'app_color' => 'App Color',
    ],
    'component' => [
        'additionalcode' => [
            'name' => 'Additional Code',
            'description' => 'HTML, CSS & JS',
        ],
    ],
];