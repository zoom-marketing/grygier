<?php namespace Grape\AdditionalCode;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Grape\AdditionalCode\Components\AdditionalCode' => 'aditionalcode'
        ];
    }

    public function registerSettings()
    {
        return [
            'base' => [
                'label'       => trans('grape.additionalcode::lang.plugin.name'),
                'description' => trans('grape.additionalcode::lang.plugin.description'),
                'category'    => 'Grape',
                'icon'        => 'icon-home',
                'order'       => 100,
                'keywords'    => '',
                'class'       => 'Grape\AdditionalCode\Models\AdditionalCode',
            ]
        ];
    }
}
