# Grape.AdditionalCode
HTML, CSS & JS

    description = "Default layout"
    [aditionalcode]
    ==
    {% component 'aditionalcode' %}
    <!DOCTYPE html>
    <html>
        <head>
            ...
            {% placeholder appIcons default %}
            <link rel="icon" type="image/png" href="{{ mix('/images/favicon.png') }}">
            {% endplaceholder %}
            ...
            {% styles %}
            {% placeholder headAfter %}
        </head>
        <body>
            {% placeholder bodyBefore %}
            ...
            {% scripts %}
            {% placeholder bodyAfter %}
        </body>
    </html>
