<?php namespace Grape\Iconsbox\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;

class IconsBoxs extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Grape.Widgets', 'widgets-main-menu');
    }
}
