<?php return [
    'plugin' => [
        'name' => 'IconsBox',
        'description' => '',
    ],
    'form' => [
        'name' => 'Nazwa',
        'is_active' => 'Aktywny',
        'icon' => 'Icona',
        'url' => 'URL',
        'id' => 'ID',
        'icons' => 'Icony',
        'uuid' => 'UUID',
        'description' => 'Opis',
    ],
    'menu' => [
        'icons' => 'Icons Box',
    ],
    'component' => [
        'iconsbox' => [
            'name' => 'Icons Box',
            'description' => 'Wyświetl icons box.',
            'param' => [
                'box' => [
                    'title' => 'Icons Box',
                    'description' => 'Wyświetl icons box',
                    'placeholder' => 'Wybierz icons box'
                ]
            ]
        ],
    ],
];
