<?php return [
    'plugin' => [
        'name' => 'IconsBox',
        'description' => '',
    ],
    'form' => [
        'name' => 'Name',
        'is_active' => 'Active',
        'icon' => 'Icon',
        'url' => 'URL',
        'id' => 'ID',
        'icons' => 'Icons',
        'uuid' => 'UUID',
        'description' => 'Description',
    ],
    'menu' => [
        'icons' => 'Icons Box',
    ],
    'component' => [
        'iconsbox' => [
            'name' => 'Icons Box',
            'description' => 'Display a icons box.',
            'param' => [
                'box' => [
                    'title' => 'Icons Box',
                    'description' => 'Display a icons box',
                    'placeholder' => 'Select box'
                ]
            ]
        ],
    ],
];
