<?php
namespace Grape\IconsBox\Components;

use Cms\Classes\ComponentBase;
use Grape\IconsBox\Models\IconsBox as IconsBoxModel;

class IconsBox extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.iconsbox::lang.component.iconsbox.name'),
            'description' => trans('grape.iconsbox::lang.component.iconsbox.description'),
        ];
    }

    public function box(string $uuid)
    {
        return IconsBoxModel::where([['is_active', true], ['uuid', $uuid]])->first();
    }
}
