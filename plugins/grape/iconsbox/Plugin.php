<?php namespace Grape\Iconsbox;

use Grape\Widgets\Classes\Traits\WidgetsExtendPluginTrait;
use System\Classes\PluginBase;
use Backend\Facades\Backend;

class Plugin extends PluginBase
{

    use WidgetsExtendPluginTrait;

    public $require = ['Grape.Widgets'];

    public function registerComponents()
    {
        return [
            'Grape\Iconsbox\Components\IconsBox' => 'iconsbox',
        ];
    }

    public function boot()
    {
        $menuItem = [
            'grape.iconsbox' => [
                'label'       => trans('grape.iconsbox::lang.menu.icons'),
                'icon'        => 'icon-th-large',
                'code'        => 'side-menu-item',
                'owner'       => 'Grape.Widgets',
                'url'         => Backend::url('grape/iconsbox/iconsboxs'),
            ],
        ];

        $this->extendMenu($menuItem);
    }
}
