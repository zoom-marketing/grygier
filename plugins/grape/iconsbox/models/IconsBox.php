<?php namespace Grape\Iconsbox\Models;

use Model;

/**
 * Model
 */
class IconsBox extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    protected $jsonable = ['icons'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_iconsbox_';

    protected $fillable = [
        'is_active',
        'name',
        'uuid'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'is_active' => 'required|boolean',
        'name' => 'required|string|min:2|max:64',
        'uuid' => 'required|string|min:2|max:64'
    ];
}
