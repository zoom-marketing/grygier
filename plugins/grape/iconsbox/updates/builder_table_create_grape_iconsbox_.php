<?php namespace Grape\Iconsbox\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeIconsbox extends Migration
{
    public function up()
    {
        Schema::create('grape_iconsbox_', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->boolean('is_active')->defalut(0);
            $table->string('name', 64);
            $table->string('uuid', 64);
            $table->text('icons')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_iconsbox_');
    }
}
