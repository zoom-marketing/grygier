<?php namespace Grape\Mix;

use System\Classes\PluginBase;
use October\Rain\Exception\SystemException;
use Cms\Classes\Theme;
use Cms\Classes\Asset;
use Carbon\Carbon;
use Config;
use Cache;

class Plugin extends PluginBase
{
    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'mix' => [$this, 'parseManifest']
            ]
        ];
    }

    protected function getManifest($theme)
    {
        $asset = Asset::load($theme, 'dist/mix-manifest.json');
        if (!$asset || is_null($asset)) {
            throw new SystemException("Couldn't find mix-manifest.json");
        }
        return json_decode($asset->content, true);
    }

    public function parseManifest(string $path): string
    {
        $theme = Theme::getActiveTheme();
        $manifestCacheKey = sprintf('%s:%s', $theme->getDirName(), 'mix-manifest');
        if (Config::get('app.debug')) {
            $manifest = $this->getManifest($theme);
        }
        else {
            $manifest = Cache::get($manifestCacheKey, function () use ($theme, $manifestCacheKey) {
                $manifest = $this->getManifest($theme);
                Cache::add($manifestCacheKey, $manifest, Carbon::now()->addHour());
                return $manifest;
            });
        }
        if (!isset($manifest[$path])) {
            $manifest[$path] = $path; // Fallback for non versioned file
        }
        return sprintf('/themes/%s/assets', $theme->getDirName()) . '/dist' . $manifest[$path];
    }
}
