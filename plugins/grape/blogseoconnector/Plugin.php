<?php namespace Grape\BlogSeoConnector;

use System\Classes\PluginBase;
use Grape\Blog\Models\Post;
use Grape\Blog\Controllers\Posts;
use Grape\Blog\Models\Category;
use Grape\Blog\Controllers\Categories;
use Grape\Seo\Classes\Traits\SeoExtendPluginTrait;

class Plugin extends PluginBase
{
    use SeoExtendPluginTrait;

    public $require = ['Grape.Blog', 'Grape.Seo'];

    public function boot()
    {

        $this->extend = [
            [
                'model' =>  Post::class,
                'controller' => Posts::class
            ],
            [
                'model' =>  Category::class,
                'controller' => Categories::class
            ]
        ];
        $this->extendModel();
        $this->extendForm();
    }
}
