<?php return [
    'plugin' => [
        'name' => 'Blog',
        'description' => 'Blog plugin.',
    ],
    'menu' => [
        'blog' => 'Blog',
        'categories' => 'Kategorie',
        'posts' => 'Wpisy',
    ],
    'form' => [
        'name' => 'Nazwa',
        'id' => 'ID',
        'uuid' => 'UUID',
        'slug' => 'Slug',
        'short_description' => 'Zajawka',
        'description' => 'Opis',
        'tags' => 'Tagi',
        'published_at' => 'Opublikowano',
        'categories' => 'Kategorie',
        'is_active' => 'Aktywny',
        'parent' => 'Rodzic',
        'author' => 'Autor',
        'created_at' => 'Utworzono',
        'updated_at' => 'Aktualizowano',
    ],
    'tabs' => [
        'basic' => 'Podstawowe',
        'galleries' => 'Galerie',
        'poster' => 'Okładka',
    ],
    'component' => [
        'posts' => [
            'name' => 'Posts',
            'description' => 'Posts Component',
        ]
    ]
];
