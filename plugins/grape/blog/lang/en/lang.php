<?php return [
    'plugin' => [
        'name' => 'Blog',
        'description' => 'Blog plugin.',
    ],
    'menu' => [
        'blog' => 'Blog',
        'categories' => 'Categories',
        'posts' => 'Posts',
    ],
    'form' => [
        'name' => 'Name',
        'id' => 'ID',
        'uuid' => 'UUID',
        'slug' => 'Slug',
        'short_description' => 'Short Description',
        'description' => 'Description',
        'tags' => 'Tags',
        'published_at' => 'Published At',
        'categories' => 'Categories',
        'is_active' => 'Active',
        'parent' => 'Parent',
        'author' => 'Author',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ],
    'tabs' => [
        'basic' => 'Basic',
        'galleries' => 'Galleries',
        'poster' => 'Poster',
    ],
    'component' => [
        'posts' => [
            'name' => 'Posts',
            'description' => 'Posts Component',
        ]
    ]
];
