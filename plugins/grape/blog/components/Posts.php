<?php
namespace Grape\Blog\Components;

use Cms\Classes\ComponentBase;
use Grape\Blog\Models\Post;

class Posts extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.blog::lang.component.posts.name'),
            'description' => trans('grape.blog::lang.component.posts.description'),
        ];
    }

    public function paginate(Int $count = 12)
    {
        return Post::isPublished()->orderBy('published_at', 'desc')->paginate($count);
    }

    public function category($uuid, bool $isSlug = false, Int $count = 12)
    {
        $model = $this->query('categories', $uuid, $isSlug);
        return $model->paginate($count);
    }

    public function randomFromCategory($uuid, bool $isSlug = false, Int $count = 12)
    {
        $model = $this->query('categories', $uuid, $isSlug);
        return $model->inRandomOrder()->limit($count)->get();
    }


    public function post(String $slug)
    {
        return Post::isPublished()->where('slug', $slug)->first();
    }

    protected function query(string $relation, $uuid, bool $isSlug = false)
    {
        $model = Post::isPublished()->orderBy('published_at', 'desc');

        $field = ($isSlug)?'slug':'uuid';

        if (!is_array($uuid)) {
            $uuid = [$uuid];
        }

        foreach ($uuid as $id) {
            $model = $model->whereHas($relation, function ($query) use ($id, $field) {
                $query->where($field, $id);
            });
        }

        return $model;
    }
}
