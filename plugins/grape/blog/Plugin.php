<?php namespace Grape\Blog;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public $require = ['Grape.Media', 'Rahman.Faker'];

    public function registerComponents()
    {
        return [
            'Grape\Blog\Components\Posts' => 'posts',
        ];
    }

    public function register()
    {
        $this->registerConsoleCommand('grape.blog.seed', 'Grape\Blog\Console\SeedCommand');
    }
}
