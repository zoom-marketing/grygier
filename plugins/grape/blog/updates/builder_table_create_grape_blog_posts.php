<?php namespace Grape\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeBlogPosts extends Migration
{
    public function up()
    {
        Schema::create('grape_blog_posts', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->string('name', 255)->nullable();
            $table->string('short_description', 255)->nullable();
            $table->text('description')->nullable();
            $table->integer('author_id')->unsigned()->nullable();
            $table->boolean('is_active')->default(0);
            $table->string('slug', 255);
            $table->string('tags', 255)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_blog_posts');
    }
}
