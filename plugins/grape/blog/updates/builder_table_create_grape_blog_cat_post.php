<?php namespace Grape\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeBlogCatPost extends Migration
{
    public function up()
    {
        Schema::create('grape_blog_cat_post', function ($table) {
            $table->engine = 'InnoDB';
            $table->integer('post_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->primary(['post_id', 'category_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_blog_cat_post');
    }
}
