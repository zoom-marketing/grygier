<?php namespace Grape\Blog\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeBlogCategories extends Migration
{
    public function up()
    {
        Schema::create('grape_blog_categories', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('uuid', 64);
            $table->string('slug', 64);
            $table->string('name', 64);
            $table->text('description')->nullable();
            $table->boolean('is_active')->default(0);
            $table->integer('parent_id')->nullable();
            $table->integer('nest_left')->nullable();
            $table->integer('nest_right')->nullable();
            $table->integer('nest_depth')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_blog_categories');
    }
}
