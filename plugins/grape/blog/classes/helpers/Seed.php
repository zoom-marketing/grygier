<?php namespace Grape\Blog\Classes\Helpers;

use Faker;
use DB;
use Grape\Blog\Models\Category;
use Grape\Blog\Models\Post;
use Backend\Models\User;

class Seed
{

    const ENV_PROD = 'prod';
    const ENV_DEV = 'dev';

    const LOCALE = 'pl_PL';
    const POST_COUNT = 100;
    const CATEGORIES_COUNT = 10;


    protected $env;

    /**
     * Seed constructor.
     * @param $env
     */
    public function __construct($env)
    {
        $this->env = $env;
    }


    public static function run($env)
    {
        $obj = new self($env);
        $obj->runAlways();
        switch ($obj->env) {
            case self::ENV_DEV:
                $obj->runDev();
                break;
            case self::ENV_PROD:
                $obj->runProd();
                break;
        }
    }

    private function runAlways()
    {
    }

    private function runDev()
    {
        $faker = Faker\Factory::create(self::LOCALE);
        for ($i = 0; $i < self::CATEGORIES_COUNT; $i++) {
            $name = $name = $faker->sentence($nbWords = 2, $variableNbWords = true);
            $items  = Category::create([
                'name' => $name,
                'uuid' => str_slug($name),
                'slug' => str_slug($name),
                'is_active' => $faker->boolean($chanceOfGettingTrue = 80),
                'description' => $faker->sentence($nbWords = 6, $variableNbWords = true)
            ]);
        }

        $categories = Category::all();
        $authors = User::all();
        for ($i = 0; $i < self::POST_COUNT; $i++) {
            $name = $faker->sentence($nbWords = 2, $variableNbWords = true);
            $post = Post::create(array(
                'name' => $name,
                'slug' => str_slug($name),
                'short_description' => $faker->paragraph($nbSentences = 2, $variableNbSentences = true),
                'description' => $faker->paragraph($nbSentences = 20, $variableNbSentences = true),
                'is_active' => $faker->boolean($chanceOfGettingTrue = 80),
                'published_at' => $faker->dateTimeBetween(
                    $startDate = '-1 years',
                    $endDate = '+1 month',
                    $timezone = null
                ),
                'tags' => implode(',', $faker->words($nb = rand(2, 5), $asText = false))
            ));

            $post->categories()->attach($categories->random(rand(1, 3)));
            $post->author()->associate($authors->random(1)->first());
            $post->save();
        }
    }

    private function runProd()
    {
    }
}
