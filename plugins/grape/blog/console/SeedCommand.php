<?php namespace Grape\Blog\Console;

use Grape\Blog\Classes\Helpers\Seed;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SeedCommand extends Command
{

    const PLUGIN_NAME = 'Grape.Blog';

    /**
     * @var string The console command name.
     */
    protected $name = 'grape:blog-seed';

    /**
     * @var string The console command description.
     */
    protected $description = 'Generating data for Grape.Blog.';

    protected $envOptions = ['prod', 'dev'];

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->info('Generating data for Grape.Blog.');

        if (!in_array($this->option('env'), $this->envOptions)) {
            throw new \Symfony\Component\Console\Exception\InvalidArgumentException('--env argument is required. (prod/dev)');
        }

        $this->call('plugin:refresh', ['name' => self::PLUGIN_NAME]);

        Seed::run($this->option('env'));

        $this->info('Finish!');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
