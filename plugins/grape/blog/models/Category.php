<?php namespace Grape\Blog\Models;

use Model;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\NestedTree;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $translatable = [
        'slug',
        'name',
        'description'
    ];

    protected $fillable = [
        'uuid',
        'slug',
        'name',
        'description',
        'is_active',
        'parent_id',
        'nest_left',
        'nest_right',
        'nest_depth'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_blog_categories';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'uuid' => 'required|string|min:2|max:64',
        'slug' => 'required|string|min:2|max:64',
        'name' => 'required|string|min:2|max:64',
        'description' => 'nullable|string',
    ];

    public $belongsToMany = [
        'posts' => ['Grape\Blog\Models\Post', 'table' => 'grape_blog_cat_post']
    ];
}
