<?php namespace Grape\Blog\Models;

use Carbon\Carbon;
use Model;
use BackendAuth;
use Grape\Media\Models\ImageResponsive;
use Grape\Media\Models\Gallery;

/**
 * Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];


    protected $dates = ['deleted_at', 'published_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_blog_posts';

    protected $fillable = [
        'name',
        'short_description',
        'description',
        'author_id',
        'is_active',
        'slug',
        'tags',
        'published_at'
    ];

    public $translatable = [
        'name',
        'short_description',
        'description',
        'tags'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|string|min:2|max:255',
        'short_description' => 'nullable|string|min:2|max:255',
        'description' => 'nullable|string',
        'author' => 'nullable|exists:backend_users,id',
        'is_active' => 'nullable|boolean',
        'slug' => 'required|string|min:2|max:255',
        'tags' => 'nullable|string|min:2|max:255',
        'published_at' => 'required|date'
    ];

    public $morphOne = [
        'poster' => [ImageResponsive::class, 'name' => 'imageable', 'delete' => true]
    ];

    public $morphMany = [
        'galleries' => [Gallery::class, 'name' => 'galleryable', 'delete' => true]
    ];

    public $belongsTo = [
        'author' => 'Backend\Models\User'
    ];

    public $belongsToMany = [
        'categories' => ['Grape\Blog\Models\Category', 'table' => 'grape_blog_cat_post']
    ];

    public function beforeCreate()
    {
        $user = BackendAuth::getUser();
        $this->author = $user;
    }

    public function afterCreate()
    {
        $this->poster = ImageResponsive::create([]);
        $this->save();
    }


    public static function getFromModel($model)
    {
        if ($model->seo) {
            return $model->seo;
        }
        $seo = new static;
        $seo->seoable()->associate($model)->save();
    }

    public function scopeIsActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeIsPublished($query)
    {
        return $query->where([['is_active', true], ['published_at', '<=', Carbon::now() ]]);
    }
}
