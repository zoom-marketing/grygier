<?php namespace Grape\Pages\Models;

use Model;
use BackendAuth;

/**
 * Model
 */
class Page extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\NestedTree;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_pages_pages';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name'];

    protected $jsonable = ['extended_fields'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|string|min:2|max:64',
        'uuid' => 'required|string|min:2|max:64|unique:grape_pages_pages',
        'author' => 'nullable|exists:backend_users,id',
        'is_active' => 'boolean'
    ];

    protected $fillable = ['name', 'uuid', 'is_active'];

    public $belongsTo = [
        'author' => 'Backend\Models\User'
    ];

    public function beforeCreate()
    {
        $user = BackendAuth::getUser();
        $this->author = $user;
    }

    public function scopeIsActive($query)
    {
        return $query->where('is_active', true);
    }
}
