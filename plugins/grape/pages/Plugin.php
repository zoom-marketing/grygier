<?php namespace Grape\Pages;

use Yaml;
use File;
use Grape\Pages\Controllers\Pages;
use Grape\Pages\Models\Page;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public $require = [];

    public function registerComponents()
    {
        return [
            'Grape\Pages\Components\Pages' => 'pages',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerFormWidgets()
    {
        return [
            'Grape\Pages\FormWidgets\ExternalLinks' => 'externallinks'
        ];
    }
    public function boot()
    {
        $this->extendForm();
    }

    protected function extendForm()
    {
        Pages::extendFormFields(function ($form, $model, $context) {
            if (!$model instanceof Page) {
                return;
            }
            if (!$model->exists) {
                return;
            }
            if ($model
                && $model->uuid
                && file_exists(plugins_path('/grape/pages/models/page/extended_fields/'.$model->uuid.'/fields.yaml'))
            ) {
                $configFile = plugins_path('grape/pages/models/page/extended_fields/'.$model->uuid.'/fields.yaml');
                $config = Yaml::parse(File::get($configFile));
                if (is_array($config)) {
                    $form->addTabFields($config);
                }
            }
        });
    }
}
