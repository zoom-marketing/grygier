<?php namespace Grape\Pages\FormWidgets;

use Yaml;
use File;
use Backend;
use Backend\Classes\FormWidgetBase;

class ExternalLinks extends FormWidgetBase
{
    protected $defaultAlias = 'externallinks';

    public function render()
    {
        $links = collect([]);

        if ($this->model
            && $this->model->uuid
            && file_exists(plugins_path('/grape/pages/models/page/extended_fields/'.$this->model->uuid.'/links.yaml'))
        ) {
            $configFile = plugins_path('grape/pages/models/page/extended_fields/'.$this->model->uuid.'/links.yaml');
            $config = Yaml::parse(File::get($configFile));
            $links = collect($config);
            $links = $links->map(function ($item) {
                if (isset($item['path'])) {
                    $item['path'] = Backend::url($item['path']);
                }
                return $item;
            });
        }

        $this->vars['links'] = $links;
        return $this->makePartial('externallinks');
    }

    public function getSaveValue($value)
    {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
}
