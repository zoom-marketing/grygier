<?php

namespace Grape\Pages\Components;

use Grape\Pages\Models\Page;
use Cms\Classes\ComponentBase;

class Pages extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.pages::lang.component.page.name'),
            'description' => trans('grape.pages::lang.component.page.description'),
        ];
    }

    public function page(string $uuid, bool $onlyIsActive = true)
    {
        $model = Page::where('uuid', $uuid);
        if ($onlyIsActive) {
            $model = $model->where('is_active', true);
        }
        return $model->first();
    }

    public function pageChildren(string $uuid, bool $onlyIsActive = true)
    {
        $model = Page::where('uuid', $uuid);
        if ($onlyIsActive) {
            $model = $model->where('is_active', true);
        }
        return $model->first()->getAllChildren()->where('is_active', true);
    }

    public function extendedField(string $uuid, string $key, bool $onlyIsActive = true)
    {
        $value = null;
        $page = $this->page($uuid, $onlyIsActive);
        if ( $page instanceof Page
            && $page->extended_fields
            && isset($page->extended_fields[$key])
        ) {
            $value = $page->extended_fields[$key];
        }

        return $value;
    }
}
