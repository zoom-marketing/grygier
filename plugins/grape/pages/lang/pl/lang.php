<?php return [
    'plugin' => [
        'name' => 'Strony',
        'description' => 'Podstawowy plugin do obsługi stron.',
    ],
    'form' => [
        'name' => 'Nazwa',
        'uuid' => 'UUID',
        'is_active' => 'Aktywny',
        'id' => 'ID',
        'author' => 'Autor',
        'externallinks' => 'Linki'
    ],
    'permission' => [
        'access' => 'Dostęp',
    ],
    'menu' => [
        'pages' => 'Strony',
    ],
    'tabs' => [
        'basic' => 'Podstawowe',
        'extended_fields' => 'Pola rozszeżone',
    ],
];
