<?php return [
    'plugin' => [
        'name' => 'Pages',
        'description' => 'Basic static pages plugin.',
    ],
    'form' => [
        'name' => 'Name',
        'uuid' => 'UUID',
        'is_active' => 'Active',
        'id' => 'ID',
        'author' => 'Author',
        'externallinks' => 'Links'
    ],
    'permission' => [
        'access' => 'Access',
    ],
    'menu' => [
        'pages' => 'Pages',
    ],
    'tabs' => [
        'basic' => 'Basic',
        'extended_fields' => 'Extended Fields',
    ],
];
