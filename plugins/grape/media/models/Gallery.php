<?php namespace Grape\Media\Models;

use Model;

/**
 * Model
 */
class Gallery extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_media_galeries';

    public $morphMany = [
        'galleryable' => []
    ];

    public $attachMany = [
        'photos' => ['System\Models\File', 'delete' => true]
    ];

    protected $fillable = [
        'uuid',
        'name',
        'description'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'uuid' => 'required|string|min:2|max:64',
        'name' => 'required|string|min:2|max:255',
        'description' => 'nullable|string'
    ];

    public $translatable = [
        'name',
        'description'
    ];





}
