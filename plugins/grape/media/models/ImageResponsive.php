<?php namespace Grape\Media\Models;

use Model;

/**
 * Model
 */
class ImageResponsive extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_media_images';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $morphOne = [
        'imageable' => []
    ];

    public $attachOne = [
        'small' => ['System\Models\File', 'delete' => true],
        'medium' => ['System\Models\File', 'delete' => true],
        'large' => ['System\Models\File', 'delete' => true],
        'xlarge' => ['System\Models\File', 'delete' => true]
    ];
}
