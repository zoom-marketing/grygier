<?php namespace Grape\Media\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Galleries extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    protected $requiredPermissions = [
        'grape.media.access'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Grape.Media', 'main-menu-item', 'side-menu-item');
    }
}
