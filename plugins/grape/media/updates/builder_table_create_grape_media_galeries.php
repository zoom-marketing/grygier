<?php namespace Grape\Media\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeMediaGaleries extends Migration
{
    public function up()
    {
        Schema::create('grape_media_galeries', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('uuid', 64);
            $table->boolean('is_active')->nullable()->default(1);
            $table->integer('galleryable_id')->nullable();
            $table->string('galleryable_type', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->text('description')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_media_galeries');
    }
}
