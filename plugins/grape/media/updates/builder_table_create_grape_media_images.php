<?php namespace Grape\Media\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeMediaImages extends Migration
{
    public function up()
    {
        Schema::create('grape_media_images', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('imageable_id')->nullable();
            $table->string('imageable_type', 255)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_media_images');
    }
}
