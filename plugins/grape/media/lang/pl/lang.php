<?php return [
    'plugin' => [
        'name' => 'Media',
        'description' => 'Images & Galleries',
    ],
    'form' => [
        'id' => 'ID',
        'name' => 'Nazwa',
        'description' => 'Opis',
        'photos' => 'Zdjęcia',
        'xlarge' => 'Extra Large',
        'large' => 'Large',
        'medium' => 'Medium',
        'small' => 'Small',
        'uuid' => 'UUID',
        'is_active' => 'Aktywny',
        'galleryable_type' => 'Model',
        'galleryable_id' => 'ID',
        'imageable_type' => 'Model',
        'imageable_id' => 'ID'
    ],
    'menu' => [
        'media' => 'Media',
        'imagesresponsive' => 'Responsive Images',
        'galleries' => 'Galleries',
    ],
    'permission' => [
        'access' => 'Pełny dostęp',
    ],
    'component' => [
        'imageresponsive' =>[
            'name' => 'Image Responsive',
            'description' => '',
        ]
    ]
];
