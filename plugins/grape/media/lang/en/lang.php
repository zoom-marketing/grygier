<?php return [
    'plugin' => [
        'name' => 'Media',
        'description' => 'Images & Galleries',
    ],
    'form' => [
        'id' => 'ID',
        'name' => 'Name',
        'description' => 'Description',
        'photos' => 'Photos',
        'xlarge' => 'Extra Large',
        'large' => 'Large',
        'medium' => 'Medium',
        'small' => 'Small',
        'uuid' => 'UUID',
        'is_active' => 'Active',
        'galleryable_type' => 'Model',
        'galleryable_id' => 'ID',
        'imageable_type' => 'Model',
        'imageable_id' => 'ID'
    ],
    'menu' => [
        'media' => 'Media',
        'imagesresponsive' => 'Responsive Images',
        'galleries' => 'Galleries',
    ],
    'permission' => [
        'access' => 'Full Access',
    ],
    'component' => [
        'imageresponsive' =>[
            'name' => 'Image Responsive',
            'description' => '',
        ]
    ]
];
