<?php namespace Grape\Media;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Grape\Media\Components\ImageResponsive' => 'imageresponsive',
        ];
    }

    public function registerSettings()
    {
    }
}
