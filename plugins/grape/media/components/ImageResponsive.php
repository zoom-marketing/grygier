<?php

namespace Grape\Media\Components;

use Cms\Classes\ComponentBase;

class ImageResponsive extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.media::lang.component.imageresponsive.name'),
            'description' => trans('grape.media::lang.component.imageresponsive.description'),
        ];
    }

    public function picture($imageResponsive = null, $crop = null, $attrPicture = '', $attrImg = '')
    {
        $content = '';
        if ($imageResponsive instanceof \Grape\Media\Models\ImageResponsive
            && $imageResponsive->xlarge) {
            $content = $this->renderPartial('@picture.htm', [
                'image' => $imageResponsive,
                'attrPicture' => $attrPicture,
                'attrImg' => $attrImg,
                'crop' => $crop,
            ]);
        }
        return $content;
    }
}
