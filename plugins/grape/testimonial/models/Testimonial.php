<?php namespace Grape\Testimonial\Models;

use Model;

/**
 * Model
 */
class Testimonial extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_testimonial_';

    protected $fillable = [
        'is_active',
        'name',
        'rating',
        'description',
        'url',
        'date',
    ];

    public $dates = ['date'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'is_active' => 'nullable|boolean',
        'name' => 'required|string|min:2|max:64',
        'rating' => 'nullable|numeric|min:1|max:5',
        'description' => 'nullable|string',
        'url' => 'nullable|url',
        'date' => 'nullable|date',
    ];

    public $attachOne = [
        'poster' => ['System\Models\File', 'delete' => true]
    ];

    public $belongsTo = [
        'testimonialGroup' => ['Grape\Testimonial\Models\TestimonialGroup']
    ];

}
