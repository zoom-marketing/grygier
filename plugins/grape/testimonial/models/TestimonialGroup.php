<?php namespace Grape\Testimonial\Models;

use Model;

/**
 * Model
 */
class TestimonialGroup extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_testimonial_group';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'is_active' => 'nullable|boolean',
        'name' => 'required|string|min:2|max:64',
        'uuid' => 'required|string|min:2|max:64'
    ];

    public $hasMany = [
        'testimonials' => ['Grape\Testimonial\Models\Testimonial', 'delete' => true]
    ];
}
