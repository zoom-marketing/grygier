<?php
namespace Grape\Testimonial\Components;

use Cms\Classes\ComponentBase;
use Grape\Testimonial\Models\TestimonialGroup;

class Testimonial extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.testimonial::lang.component.testimonial.name'),
            'description' => trans('grape.testimonial::lang.component.testimonial.description'),
        ];
    }

    public function testimonialGroup(string $uuid)
    {
        return TestimonialGroup::where([
            ['is_active', true],
            ['uuid', $uuid]
        ])->with(['testimonials' => function ($query) {
            $query->where('is_active', true);
        }])->first();
    }
}
