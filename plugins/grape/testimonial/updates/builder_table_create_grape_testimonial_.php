<?php namespace Grape\Testimonial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeTestimonial extends Migration
{
    public function up()
    {
        Schema::create('grape_testimonial_', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('testimonial_group_id')->unsigned();
            $table->foreign('testimonial_group_id')->references('id')->on('grape_testimonial_group');
            $table->boolean('is_active')->nullable();
            $table->string('name', 64)->nullable();
            $table->smallInteger('rating')->default(1);
            $table->text('description')->nullable();
            $table->string('url')->nullable();
            $table->date('date')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_testimonial_');
    }
}
