<?php namespace Grape\Testimonial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeTestimonialGroup extends Migration
{
    public function up()
    {
        Schema::create('grape_testimonial_group', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->boolean('is_active')->default(0);
            $table->string('name', 64);
            $table->string('uuid', 64);
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_testimonial_group');
    }
}
