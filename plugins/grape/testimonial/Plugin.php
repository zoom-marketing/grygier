<?php namespace Grape\Testimonial;

use System\Classes\PluginBase;
use Backend\Facades\Backend;
use Grape\Widgets\Classes\Traits\WidgetsExtendPluginTrait;

class Plugin extends PluginBase
{
    use WidgetsExtendPluginTrait;

    public $require = ['Grape.Widgets'];

    public function registerComponents()
    {
        return [
            'Grape\Testimonial\Components\Testimonial' => 'testimonial',
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        $menuItem = [
            'grape.testimonial_group' => [
                'label'       => trans('grape.testimonial::lang.menu.testimonials'),
                'icon'        => 'icon-align-center',
                'code'        => 'side-menu-item',
                'owner'       => 'Grape.Widgets',
                'url'         => Backend::url('grape/testimonial/testimonialgroups'),
            ]
        ];

        $this->extendMenu($menuItem);
    }
}
