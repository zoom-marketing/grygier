<?php return [
    'plugin' => [
        'name' => 'Opinie',
        'description' => 'Opinie klientów',
    ],
    'form' => [
        'id' => 'ID',
        'name' => 'Nazwa',
        'is_active' => 'Akrywny',
        'rating' => 'Ocena',
        'description' => 'Opis',
        'url' => 'URL',
        'date' => 'Data',
        'uuid' => 'UUID',
        'poster' => 'Okładka',
    ],
    'menu' => [
        'testimonials' => 'Opinie',
    ],
    'component' => [
        'testimonial' => [
            'name' => 'Grupa opinii',
            'description' => 'Grupa opinii'
        ],
    ],
];
