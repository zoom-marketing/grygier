<?php return [
    'plugin' => [
        'name' => 'Testimonial',
        'description' => 'Clients testimonial',
    ],
    'form' => [
        'id' => 'ID',
        'name' => 'Name',
        'is_active' => 'Active',
        'rating' => 'Rating',
        'description' => 'Description',
        'url' => 'URL',
        'date' => 'Date',
        'uuid' => 'UUID',
        'poster' => 'Poster',
    ],
    'menu' => [
        'testimonials' => 'Testimonials',
    ],
    'component' => [
        'testimonial' => [
            'name' => 'Testimonial Group',
            'description' => 'Display a testimonials group.'
        ],
    ],
];
