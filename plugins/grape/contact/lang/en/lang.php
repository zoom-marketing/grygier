<?php return [
    'plugin' => [
        'name' => 'Contact',
        'description' => 'Basic Contact plugin.',
    ],
    'form' => [
        'name' => 'Name',
        'address' => 'Address',
        'city' => 'City',
        'emails' => 'Emails',
        'emails_uuid' => 'UUID',
        'emails_value' => 'Value',
        'emails_label' => 'Label',
        'phones' => 'Phones',
        'phones_uuid' => 'UUID',
        'phones_value' => 'Value',
        'phones_label' => 'Label',
        'hours' => 'Hours',
        'hours_uuid' => 'UUID',
        'hours_label' => 'Label',
        'hours_value' => 'Value',
        'tax_number' => 'Tax number',
        'registration_number' => 'Registration number',
        'post_code' => 'Post code',
        'banks' => 'Banks',
        'banks_uuid' => 'UUID',
        'banks_name' => 'Name',
        'banks_number' => 'Number',
        'map_iframe' => 'Map Code',
        'id' => 'ID',
        'uuid' => 'UUID',
        'country' => 'Country',
        'state' => 'State',
        'is_active' => 'Active',
    ],
    'settings' => [
        'social_media' => [
            'description' => 'Set social media links',
        ],
    ],
    'permission' => [
        'tab' => 'Contact',
        'label' => [
            'access' => 'Access to Contact plugin',
        ],
    ],
    'menu' => [
        'contact' => 'Contact',
    ],
    'component' => [
        'places' => [
            'name' => 'Places',
            'description' => 'Paces component.',
        ],
        'socialmedia' => [
            'name' => 'SocialMedia',
            'description' => 'SocialMedia component.',
        ],
    ],
];