<?php return [
    'plugin' => [
        'name' => 'Kontakt',
        'description' => 'Podstawowy plugin do obsługi kontaktu.',
    ],
    'form' => [
        'name' => 'Nazwa',
        'address' => 'Adres',
        'city' => 'Miejscowość',
        'emails' => 'Emaile',
        'emails_uuid' => 'UUID',
        'emails_value' => 'Wartość',
        'emails_label' => 'Etykieta',
        'phones' => 'Telefony',
        'phones_uuid' => 'UUID',
        'phones_value' => 'Wartość',
        'phones_label' => 'Etykieta',
        'hours' => 'Godziny',
        'hours_uuid' => 'UUID',
        'hours_label' => 'Etykieta',
        'hours_value' => 'Wartość',
        'tax_number' => 'NIP',
        'registration_number' => 'REGON',
        'post_code' => 'Kod pocztowy',
        'banks' => 'Banki',
        'banks_uuid' => 'UUID',
        'banks_name' => 'Nazwa',
        'banks_number' => 'Numer konta',
        'map_iframe' => 'Kod Mapy',
        'id' => 'ID',
        'uuid' => 'UUID',
        'country' => 'Kraj',
        'state' => 'Województwo',
    ],
    'settings' => [
        'social_media' => [
            'description' => 'Ustaw linki social mediów'
        ]
    ],
    'permission' => [
        'tab' => 'Kontakt',
        'label' => [
            'access' => 'Dostęp do pluginu Kontakt',
        ],
    ],
    'menu' => [
        'contact' => 'Kontakt',
    ],
    'component' => [
        'places' => [
            'name' => 'Lokalizacje',
            'description' => 'Komponent do lokalizacji.',
        ],
        'socialmedia' => [
            'name' => 'SocialMedia',
            'description' => 'Komponent do social mediów',
        ]
    ]
];
