<?php namespace Grape\Contact\Models;

use Model;

/**
 * Model
 */
class Place extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\Sortable;

    public $implement = ['RainLab.Location.Behaviors.LocationModel'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_contact_places';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|string|min:2|max:64',
        'uuid' => 'required|string|min:2|max:64|unique:grape_contact_places',
    ];

    public $jsonable = [
        'emails', 'phones', 'hours', 'banks'
    ];

    public $belongsTo = [
        'state' => ['RainLab\Location\Models\State'],
        'country' => ['RainLab\Location\Models\Country']
    ];
}
