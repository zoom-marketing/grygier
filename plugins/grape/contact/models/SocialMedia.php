<?php namespace Grape\Contact\Models;

use Model;

class SocialMedia extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'grape_contact_social_media';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
