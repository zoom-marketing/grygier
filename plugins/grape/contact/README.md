# Contact Plugin
  * Plugin Namespace `Grape\Contact`
  * `Place Model`
    * `name`
    * `address`
    * `post_code`
    * `city`
    * `state` (hasOne: `RainLab\Location\Models\State`)
    * `country` (hasOne: `RainLab\Location\Models\Country`)
    * `emails`
      * `id`
      * `label`
      * `value`
    * `phones`
      * `id`
      * `label`
      * `value`
    * `hours`
      * `value`
    * `tax_number`
    * `registration_number`
    * `banks`
      * `id`
      * `name`
      * `number`
    * `map_iframe`
  ## Component
  ### `Grape\Contact\Component\SocialMedia`
  #### Component methods
  * `socialMedia():Collection`
    * returns all not empty social media links from SocialMedia setting model
  #### Component Examples
  ```
  [socialmedia]
  ==
  {% set socialmedia = socialmedia.socialMedia() %}
  <ul>
    {% for key, item in socialmedia %}
        <li>
            <a href="{{item}}" target="_blank">
                <i class="social-icon social-icon--{{key}}"></i>
            </a>
        </li>
    {% endfor %}
  </ul>
  ```
  ```
    .social-icon{
        @extend .fa;
        &--facebook{
            @extend .fa-facebook;
        }
        &--twitter{
            @extend .fa-twitter;
        }
        &--youtube{
            @extend .fa-youtube-play;
        }
        &--instagram{
            @extend .fa-instagram;
        }
        &--linkedin{
            @extend .fa-linkedin;
        }
    }
  ```
