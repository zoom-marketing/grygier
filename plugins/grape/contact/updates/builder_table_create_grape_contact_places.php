<?php namespace Grape\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeContactPlaces extends Migration
{
    public function up()
    {
        Schema::create('grape_contact_places', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('uuid', 64);
            $table->boolean('is_active')->default(0);
            $table->string('name', 64);
            $table->string('address', 64)->nullable();
            $table->string('city', 64)->nullable();
            $table->integer('country_id')->nullable()->unsigned();
            $table->text('emails')->nullable();
            $table->text('phones')->nullable();
            $table->integer('state_id')->nullable()->unsigned();
            $table->text('hours')->nullable();
            $table->string('tax_number', 32)->nullable();
            $table->string('registration_number', 32)->nullable();
            $table->string('post_code', 16)->nullable();
            $table->text('banks')->nullable();
            $table->text('map_iframe')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('sort_order')->unsigned()->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_contact_places');
    }
}
