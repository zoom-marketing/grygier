<?php

namespace Grape\Contact\Components;

use Grape\Contact\Models\Place;
use Cms\Classes\ComponentBase;

class Places extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.contact::lang.component.places.name'),
            'description' => trans('grape.contact::lang.component.places.description'),
        ];
    }

    public function place(string $uuid, bool $onlyIsActive = true)
    {
        $model = Place::where('uuid', $uuid);
        if ($onlyIsActive) {
            $model = $model->where('is_active', true);
        }
        return $model->first();
    }
    public function places()
    {
        $model = new Place();
        return $model->all();
    }

    public function placeFiled(string $uuid, string $key = null, string $subUuid = null, bool $onlyIsActive = true)
    {
        $value = null;
        $record = $this->place($uuid, $onlyIsActive);
        if (!$record) {
            return null;
        }

        if (isset($record->{$key})) {
            $filed = $record->{$key};

            if ($subUuid && is_array($filed)) {
                $filed = collect($filed);
                $value = $filed->filter(function ($item) use ($subUuid) {
                    return $item['uuid'] === $subUuid;
                })->first();
            } else {
                $value = $filed;
            }
        }

        return $value;
    }
}
