<?php

namespace Grape\Contact\Components;

use Cms\Classes\ComponentBase;
use Grape\Contact\Models\SocialMedia as SocialMediaModel;
use Illuminate\Support\Collection;

class SocialMedia extends ComponentBase
{
    private $fields = [
        'facebook',
        'twitter',
        'youtube',
        'instagram',
        'linkedin',
        'whatsapp'
    ];

    public function componentDetails()
    {
        return [
            'name' => trans('grape.contact::lang.component.socialmedia.name'),
            'description' => trans('grape.contact::lang.component.socialmedia.description'),
        ];
    }

    /**
     * @return Collection
     */
    public function socialMedia():Collection
    {
        $items = collect([]);
        foreach ($this->fields as $field) {
            if ($item = SocialMediaModel::get($field)) {
                $items->put($field, $item);
            }
        }
        return $items;
    }
}
