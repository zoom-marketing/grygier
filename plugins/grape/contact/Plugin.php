<?php namespace Grape\Contact;

use RainLab\Location\Models\State;
use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase
{
    public $require = ['RainLab.Location'];

    public function registerComponents()
    {
        return [
            'Grape\Contact\Components\Places' => 'places',
            'Grape\Contact\Components\SocialMedia' => 'socialmedia',
        ];
    }

    public function registerSettings()
    {
        return [
            'location' => [
                'label'       => 'Social Media',
                'description' => trans('grape.contact::lang.settings.social_media.description'),
                'category'    => 'Grape',
                'icon'        => 'icon-at',
                'order'       => 500,
                'class'       => 'Grape\Contact\Models\SocialMedia',
                'keywords'    => ''
            ]
        ];
    }
}
