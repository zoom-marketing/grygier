<?php
namespace Grape\FlashMessages\Components;

use Cms\Classes\ComponentBase;

class Flash extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.flashmessages::lang.plugin.name'),
            'description' => trans('grape.flashmessages::lang.plugin.description'),
        ];
    }

    public function onRun()
    {
        $this->addJs('/plugins/grape/flashmessages/assets/javascript/flash.js');
        $this->addCss('/plugins/grape/flashmessages/assets/css/flash.css');
    }
}
