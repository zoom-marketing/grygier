<?php namespace Grape\FlashMessages;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Grape\FlashMessages\Components\Flash' => 'flash',
        ];
    }

    public function registerSettings()
    {
    }
}
