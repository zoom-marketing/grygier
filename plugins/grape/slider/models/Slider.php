<?php namespace Grape\Slider\Models;

use Model;

/**
 * Model
 */
class Slider extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_slider_';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'    => 'required|string|min:2|max:64',
        'uuid'    => 'required|string|min:2|max:64',
        'is_active'    => 'nullable|boolean',
    ];

    public $hasMany = [
        'sliderItems' => 'Grape\Slider\Models\SliderItem'
    ];
}
