<?php namespace Grape\Slider\Models;

use Grape\Media\Models\ImageResponsive;
use Model;

/**
 * Model
 */
class SliderItem extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_slider_items';

    public $jsonable = ['content'];

    protected $fillable = [
        'name',
        'is_active',
        'content',
        'sort_order'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'    => 'required|string|min:2|max:64',
        'is_active'    => 'nullable|boolean',
        'content'    => 'nullable',
        'sort_order'    => 'required|numeric',
    ];

    public $morphOne = [
        'poster' => [ImageResponsive::class, 'name' => 'imageable']
    ];

    public $belongsTo = [
        'slider' => 'Grape\Slider\Models\Slider'
    ];

    public function afterCreate()
    {
        $this->poster = ImageResponsive::create([]);
        $this->save();
    }
}
