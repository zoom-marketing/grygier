# Grape.Slider
    [slider]
    slider = "home"
    [imageresponsive]
    ==
    {% if slider.slider() %}
        {% for slide in slider.slider().sliderItems %}
            {% if slide.poster %}
                {{ imageresponsive.picture(slide.poster)|raw }}
            {% endif %}
        {% endfor %}
    {% endif %}
