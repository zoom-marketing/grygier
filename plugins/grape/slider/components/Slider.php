<?php

namespace Grape\Slider\Components;

use Cms\Classes\ComponentBase;
use Grape\Slider\Models\Slider as SliderModel;

class Slider extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.slider::lang.component.slider.name'),
            'description' => trans('grape.slider::lang.component.slider.description'),
        ];
    }


    public function defineProperties()
    {
        return [
            'slider' => [
                'title'             => trans('grape.slider::lang.component.slider.param.slider.title'),
                'description'       => trans('grape.slider::lang.component.slider.param.slider.description'),
                'type'              => 'dropdown',
                'placeholder'       => trans('grape.slider::lang.component.slider.param.slider.placeholder'),
                'required'          => true
            ]
        ];
    }

    public function slider()
    {
        $slider = null;
        if ($this->property('slider')) {
            $slider = SliderModel::where([['is_active', true], ['uuid', $this->property('slider')]])
                ->with(['sliderItems' => function ($query) {
                    $query->where('is_active', true)->orderBy('sort_order', 'desc');
                }])
                ->first();
        }
        return $slider;
    }

    public function getSliderOptions()
    {
        return  SliderModel::all()->pluck('name', 'uuid')->toArray();
    }
}
