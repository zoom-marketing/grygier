<?php namespace Grape\Slider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeSlider extends Migration
{
    public function up()
    {
        Schema::create('grape_slider_', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 64);
            $table->string('uuid', 64);
            $table->boolean('is_active')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_slider_');
    }
}
