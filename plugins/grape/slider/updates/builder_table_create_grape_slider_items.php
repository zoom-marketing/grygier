<?php namespace Grape\Slider\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeSliderItems extends Migration
{
    public function up()
    {
        Schema::create('grape_slider_items', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 64);
            $table->integer('slider_id')->unsigned();
            $table->integer('sort_order')->default(0);
            $table->text('content')->nullable();
            $table->boolean('is_active')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_slider_items');
    }
}
