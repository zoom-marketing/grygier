<?php namespace Grape\Slider;

use System\Classes\PluginBase;
use Grape\Widgets\Classes\Traits\WidgetsExtendPluginTrait;
use Backend\Facades\Backend;

class Plugin extends PluginBase
{
    use WidgetsExtendPluginTrait;

    public $require = ['Grape.Widgets'];

    public function registerComponents()
    {
        return [
            'Grape\Slider\Components\Slider' => 'slider',
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        $menuItem = [
            'grape.slider' => [
                'label'       => trans('grape.slider::lang.menu.slider'),
                'icon'        => 'icon-picture-o',
                'code'        => 'side-menu-item',
                'owner'       => 'Grape.Widgets',
                'url'         => Backend::url('grape/slider/sliders'),
            ]
        ];

        $this->extendMenu($menuItem);
    }
}
