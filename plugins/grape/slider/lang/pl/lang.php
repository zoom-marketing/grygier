<?php return [
    'plugin' => [
        'name' => 'Slider',
        'description' => 'Slider plugin.',
    ],
    'form' => [
        'name' => 'Nazwa',
        'photos' => 'Zdjęcia',
        'is_active' => 'Aktywny',
        'uuid' => 'UUID',
        'content' => 'Zawartość',
        'size' => 'Rozmiar',
        'id' => 'ID',
        'sort_order' => 'Numer porządkowy',
        'sort_order_comment' => 'Większa wartość zostanie wyświetlona jako pierwsza.',
    ],
    'menu' => [
        'slider' => 'Slider',
    ],
    'component' => [
        'slider' => [
            'name' => 'Slider',
            'description' => 'Display a slider.',
            'param' => [
                'slider' => [
                    'title' => 'Slider',
                    'description' => 'Select slider',
                    'validation' => 'Please choose a slider...',
                    'placeholder' => 'Select slider',
                ]
            ],
        ],
    ],
];
