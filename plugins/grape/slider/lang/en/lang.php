<?php return [
    'plugin' => [
        'name' => 'Slider',
        'description' => 'A simple slider.',
    ],
    'form' => [
        'name' => 'Name',
        'photos' => 'Photos',
        'is_active' => 'Active',
        'uuid' => 'UUID',
        'content' => 'Content',
        'size' => 'Size',
        'id' => 'ID',
        'sort_order' => 'Sort Order',
        'sort_order_comment' => 'The largest value is displayed first.',
    ],
    'menu' => [
        'slider' => 'Slider',
    ],
    'component' => [
        'slider' => [
            'name' => 'Slider',
            'description' => 'Display a slider.',
            'param' => [
                'slider' => [
                    'title' => 'Slider',
                    'description' => 'Select slider',
                    'validation' => 'Please choose a slider...',
                    'placeholder' => 'Select slider',
                ]
            ],
        ],
    ],
];
