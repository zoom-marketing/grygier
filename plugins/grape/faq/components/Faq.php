<?php
namespace Grape\Faq\Components;

use Cms\Classes\ComponentBase;
use Grape\Faq\Models\Faq as FaqModel;

class Faq extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.faq::lang.component.faq.name'),
            'description' => trans('grape.faq::lang.component.faq.description'),
        ];
    }

    public function faq()
    {
        return FaqModel::where('is_active', true)->orderBy('sort_order')->get();
    }
}
