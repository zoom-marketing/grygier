<?php namespace Grape\Faq\Classes\Helpers;

use Faker;
use Grape\Faq\Models\Faq;

class Seed
{

    const ENV_PROD = 'prod';
    const ENV_DEV = 'dev';

    const LOCALE = 'pl_PL';
    const COUNT = 20;

    protected $env;

    /**
     * Seed constructor.
     * @param $env
     */
    public function __construct($env)
    {
        $this->env = $env;
    }


    public static function run($env)
    {
        $obj = new self($env);
        $obj->runAlways();
        switch ($obj->env) {
            case self::ENV_DEV:
                $obj->runDev();
                break;
            case self::ENV_PROD:
                $obj->runProd();
                break;
        }
    }

    private function runAlways()
    {
    }

    private function runDev()
    {
        $faker = Faker\Factory::create(self::LOCALE);
        for ($i = 0; $i < self::COUNT; $i++) {
            $items  = Faq::create([
                'name' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                'answer' => $faker->paragraphs($nb = 3, $asText = true),
                'is_active' => $faker->boolean($chanceOfGettingTrue = 80)
            ]);
        }
    }

    private function runProd()
    {
    }
}
