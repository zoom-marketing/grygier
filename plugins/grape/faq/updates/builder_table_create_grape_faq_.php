<?php namespace Grape\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeFaq extends Migration
{
    public function up()
    {
        Schema::create('grape_faq_', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 255);
            $table->text('answer')->nullable();
            $table->boolean('is_active')->default(0);
            $table->integer('sort_order')->nullable()->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('grape_faq_');
    }
}
