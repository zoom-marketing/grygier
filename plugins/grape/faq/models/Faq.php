<?php namespace Grape\Faq\Models;

use Model;

/**
 * Model
 */
class Faq extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_faq_';

    protected $fillable = [
        'name',
        'answer',
        'is_active',
        'sort_order'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'    => 'required|string|min:2|max:255',
        'answer'    => 'nullable|string',
        'is_active'    => 'nullable|boolean',
        'sort_order'    => 'nullable|numeric',
    ];
}
