<?php namespace Grape\Faq\Console;

use Grape\Faq\Classes\Helpers\Seed;
use Illuminate\Console\Command;

class SeedCommand extends Command
{

    const PLUGIN_NAME = 'Grape.Faq';

    /**
     * @var string The console command name.
     */
    protected $name = 'grape:faq-seed';

    /**
     * @var string The console command description.
     */
    protected $description = 'Generating data for Grape.Faq.';

    protected $envOptions = ['prod', 'dev'];

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->info('Generating data for Grape.Faq.');

        if (!in_array($this->option('env'), $this->envOptions)) {
            throw new \Symfony\Component\Console\Exception\InvalidArgumentException('--env argument is required. (prod/dev)');
        }

        $this->call('plugin:refresh', ['name' => self::PLUGIN_NAME]);

        Seed::run($this->option('env'));

        $this->info('Finish!');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
