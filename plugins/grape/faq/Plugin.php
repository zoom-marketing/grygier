<?php namespace Grape\Faq;

use Backend\Facades\Backend;
use Grape\Widgets\Classes\Traits\WidgetsExtendPluginTrait;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    use WidgetsExtendPluginTrait;

    public $require = ['Grape.Widgets'];

    public function registerComponents()
    {
        return [
            'Grape\Faq\Components\Faq' => 'faq',
        ];
    }

    public function registerSettings()
    {
    }

    public function register()
    {
        $this->registerConsoleCommand('grape.faq.seed', 'Grape\Faq\Console\SeedCommand');
    }

    public function boot()
    {
        $menuItem = [
            'grape.faq' => [
                'label'       => trans('grape.faq::lang.menu.faq'),
                'icon'        => 'oc-icon-question-circle',
                'code'        => 'side-menu-item',
                'owner'       => 'Grape.Widgets',
                'url'         => Backend::url('grape/faq/faqs'),
            ]
        ];

        $this->extendMenu($menuItem);
    }
}
