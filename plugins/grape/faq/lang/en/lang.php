<?php return [
    'plugin' => [
        'name' => 'Faq',
        'description' => 'FAQ Plugin.',
    ],
    'form' => [
        'name' => 'Question',
        'is_active' => 'Active',
        'answer' => 'Answer',
        'id' => 'ID',
    ],
    'menu' => [
        'faq' => 'FAQ'
    ],
    'component' => [
        'faq' => [
            'name' => 'FAQ',
            'description' => 'FAQ Plugin.'
        ]
    ]
];
