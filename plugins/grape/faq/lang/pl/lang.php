<?php return [
    'plugin' => [
        'name' => 'Faq',
        'description' => 'FAQ Plugin.',
    ],
    'form' => [
        'name' => 'Pytanie',
        'is_active' => 'Aktywne',
        'answer' => 'Odpowiedź',
        'id' => 'ID',
    ],
    'menu' => [
        'faq' => 'FAQ'
    ],
    'component' => [
        'faq' => [
            'name' => 'FAQ',
            'description' => 'FAQ Plugin.'
        ]
    ]
];
