<?php
namespace Grape\Documents\Components;

use Cms\Classes\ComponentBase;
use Grape\Documents\Models\DocumentGroup;

class Documents extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.documents::lang.component.documents.name'),
            'description' => trans('grape.documents::lang.component.documents.description'),
        ];
    }


    public function defineProperties()
    {
        return [
            'documents' => [
                'title'             => trans('grape.documents::lang.component.documents.param.documents.title'),
                'description'       => trans('grape.documents::lang.component.documents.param.documents.description'),
                'type'              => 'dropdown',
                'placeholder'       => trans('grape.documents::lang.component.documents.param.documents.placeholder'),
                'required'          => true
            ]
        ];
    }


    public function documentGroups()
    {
        return DocumentGroup::where([['is_active', true]])->get();
    }

    public function documentGroup()
    {
        $documents = null;
        if ($this->property('documents')) {
            $documents = DocumentGroup::where([['is_active', true], ['uuid', $this->property('documents')]])->first();
        }
        return $documents;
    }

    public function getDocumentsOptions()
    {
        return  DocumentGroup::all()->pluck('name', 'uuid')->toArray();
    }
}
