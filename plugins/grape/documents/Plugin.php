<?php namespace Grape\Documents;

use Backend\Facades\Backend;
use Grape\Widgets\Classes\Traits\WidgetsExtendPluginTrait;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    use WidgetsExtendPluginTrait;

    public $require = ['Grape.Widgets'];

    public function registerComponents()
    {
        return [
            'Grape\Documents\Components\Documents' => 'documents',
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        $menuItem = [
            'grape.documents' => [
                'label'       => trans('grape.documents::lang.menu.documents'),
                'icon'        => 'oc-icon-file-word-o',
                'code'        => 'side-menu-item',
                'owner'       => 'Grape.Widgets',
                'url'         => Backend::url('grape/documents/documentgroups'),
            ]
        ];

        $this->extendMenu($menuItem);
    }
}
