<?php namespace Grape\Documents\Models;

use Model;

/**
 * Model
 */
class DocumentGroup extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    protected $jsonable = ['documents'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'grape_documents_group';

    protected $fillable = [
        'name',
        'uuid',
        'is_active',
        'description',
        'sort_order',
        'documents'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name'    => 'required|string|min:2|max:64',
        'uuid'    => 'required|string|min:2|max:64',
        'is_active'    => 'nullable|boolean',
        'description'    => 'nullable|string',
        'sort_order'    => 'nullable|numeric',
        'documents'    => 'nullable'
    ];
}
