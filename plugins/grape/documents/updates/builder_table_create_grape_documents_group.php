<?php namespace Grape\Documents\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGrapeDocumentsGroup extends Migration
{
    public function up()
    {
        Schema::create('grape_documents_group', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 64);
            $table->string('uuid', 64);
            $table->text('description')->nullable();
            $table->boolean('is_active')->default(0);
            $table->integer('sort_order')->nullable();
            $table->text('documents')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('grape_documents_group');
    }
}
