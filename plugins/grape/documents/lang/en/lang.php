<?php return [
    'plugin' => [
        'name' => 'Documents',
        'description' => 'Documents Plugin.',
    ],
    'form' => [
        'is_active' => 'Active',
        'name' => 'Name',
        'uuid' => 'UUID',
        'description' => 'Description',
        'documents' => 'Documents',
        'add_documents' => 'Add document',
        'icon' => 'Icon',
        'file' => 'File',
    ],
    'menu' => [
        'documents' => 'Documents'
    ],
    'component' => [
        'documents' => [
            'name' => 'Documents',
            'description' => 'Documents component',
            'param' => [
                'documents' => [
                    'title' => 'Documents Group',
                    'description' => 'Select documents group',
                    'placeholder' => 'Select documents group.',
                ]
            ]
        ]
    ]
];
