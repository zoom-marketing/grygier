<?php return [
    'plugin' => [
        'name' => 'Dokumenty',
        'description' => 'Plugin do dokumentów',
    ],
    'form' => [
        'is_active' => 'Aktywna',
        'name' => 'Nazwa',
        'uuid' => 'UUID',
        'description' => 'Opis',
        'documents' => 'Dokumenty',
        'add_documents' => 'Dodaj document',
        'icon' => 'Ikona',
        'file' => 'Plik',
    ],
    'menu' => [
        'documents' => 'Dokumenty'
    ],
    'component' => [
        'documents' => [
            'name' => 'Documents',
            'description' => 'Documents component',
            'param' => [
                'documents' => [
                    'title' => 'Documents Group',
                    'description' => 'Select documents group',
                    'placeholder' => 'Select documents group.',
                ]
            ]
        ]
    ]
];
