<?php return [
    'plugin' => [
        'name' => 'Shopaholic Seo Connector',
        'description' => 'Integracja Shopaholic z Grape.Seo'
    ],
    'component' => [
        'seo' => [
            'name' => 'Shopaholic Seo Connector',
            'description' => 'Extending the Shopaholic plugin to Seo'
        ]
    ]

];
