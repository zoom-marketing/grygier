<?php return [
    'plugin' => [
        'name' => 'Shopaholic Seo Connector',
        'description' => 'Extending the Shopaholic plugin to Seo'
    ],
    'component' => [
        'seo' => [
            'name' => 'Shopaholic Seo Connector',
            'description' => 'Extending the Shopaholic plugin to Seo'
        ]
    ]
];
