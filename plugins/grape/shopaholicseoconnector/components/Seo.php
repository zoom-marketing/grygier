<?php
namespace Grape\ShopaholicSeoConnector\Components;

use Cms\Classes\ComponentBase;
use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Models\Product;
use Grape\Seo\Models\Seo as SeoModel;

class Seo extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => trans('grape.shopaholicseoconnector::lang.component.seo.name'),
            'description' => trans('grape.shopaholicseoconnector::lang.component.seo.description'),
        ];
    }

    public function getProduct($product)
    {
        $seo = null;
        if ($product && $product->id) {
            $product = Product::find($product->id);
            if ($product instanceof Product && $product->seo instanceof SeoModel) {
                $seo = $product->seo;
            }
        }
        return $seo;
    }

    public function getCategory($category)
    {
        $seo = null;
        if ($category && $category->id) {
            $category = Category::find($category->id);
            if ($category instanceof Category && $category->seo instanceof SeoModel) {
                $seo = $category->seo;
            }
        }
        return $seo;
    }
}
