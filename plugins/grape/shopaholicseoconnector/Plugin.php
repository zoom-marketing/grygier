<?php namespace Grape\ShopaholicSeoConnector;

use System\Classes\PluginBase;
use Lovata\Shopaholic\Models\Product;
use Lovata\Shopaholic\Controllers\Products;
use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Controllers\Categories;

use Grape\Seo\Classes\Traits\SeoExtendPluginTrait;

class Plugin extends PluginBase
{
    use SeoExtendPluginTrait;

    public $require = ['Lovata.Shopaholic', 'Grape.Seo'];

    public function registerComponents()
    {
        return [
            'Grape\ShopaholicSeoConnector\Components\Seo' => 'shopaholicSeo'
        ];
    }

    public function boot()
    {

        $this->extend = [
            [
                'model' => Product::class,
                'controller' => Products::class
            ],
            [
                'model' =>  Category::class,
                'controller' => Categories::class
            ]
        ];
        $this->extendModel();
        $this->extendForm();
    }
}
