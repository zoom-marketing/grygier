<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/partials/components/navbar-contact.htm */
class __TwigTemplate_68f9044a70d47c249e611e0bbf63833cde39110ba104063258e0defb53566d14 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["place"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "place", [0 => "primary"], "method", false, false, false, 1);
        // line 2
        if (($context["place"] ?? null)) {
            // line 3
            echo "    ";
            $context["phone"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "phones", 2 => "primary"], "method", false, false, false, 3);
            // line 4
            echo "    ";
            $context["email"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "emails", 2 => "primary"], "method", false, false, false, 4);
            // line 5
            echo "    ";
            $context["address"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "address"], "method", false, false, false, 5);
            // line 6
            echo "    ";
            $context["city"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "city"], "method", false, false, false, 6);
        }
        // line 8
        echo "<nav class=\"navbar-contact uk-dark\">
    <div class=\"container-inside navbar-contact__container\">
        ";
        // line 10
        if ( !(null === ($context["place"] ?? null))) {
            // line 11
            echo "        <ul class=\"uk-iconnav uk-flex-between\">
            ";
            // line 12
            if ( !(null === ($context["phone"] ?? null))) {
                echo "<li><a href=\"tel:";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["phone"] ?? null), "value", [], "any", false, false, false, 12), "html", null, true);
                echo "\"><span data-uk-icon=\"icon: phone\"></span> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["phone"] ?? null), "value", [], "any", false, false, false, 12), "html", null, true);
                echo "</a></li>";
            }
            // line 13
            echo "            ";
            if ( !(null === ($context["email"] ?? null))) {
                echo "<li><a href=\"mailto:";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["email"] ?? null), "value", [], "any", false, false, false, 13), "html", null, true);
                echo "\"><span data-uk-icon=\"icon: mail\"></span> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["email"] ?? null), "value", [], "any", false, false, false, 13), "html", null, true);
                echo "</a></li>";
            }
            // line 14
            echo "            ";
            if ( !(null === ($context["address"] ?? null))) {
                echo "<li class=\"uk-visible@s\"><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
                echo "#maps\" ";
                if ((($context["mode"] ?? null) != "dark")) {
                    echo " data-uk-scroll=\"offset: 135\"";
                }
                echo "><span data-uk-icon=\"icon: location\"></span>";
                echo twig_escape_filter($this->env, ($context["city"] ?? null), "html", null, true);
                echo ", ";
                echo twig_escape_filter($this->env, ($context["address"] ?? null), "html", null, true);
                echo "</a></li>";
            }
            // line 15
            echo "        </ul>
        ";
        }
        // line 17
        echo "    </div>
</nav>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/partials/components/navbar-contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 17,  95 => 15,  80 => 14,  71 => 13,  63 => 12,  60 => 11,  58 => 10,  54 => 8,  50 => 6,  47 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set place = places.place('primary') %}
{% if place %}
    {% set phone = places.placeFiled('primary', 'phones', 'primary') %}
    {% set email = places.placeFiled('primary', 'emails', 'primary') %}
    {% set address = places.placeFiled('primary', 'address') %}
    {% set city = places.placeFiled('primary', 'city') %}
{% endif %}
<nav class=\"navbar-contact uk-dark\">
    <div class=\"container-inside navbar-contact__container\">
        {% if place is not null %}
        <ul class=\"uk-iconnav uk-flex-between\">
            {% if phone is not null %}<li><a href=\"tel:{{ phone.value }}\"><span data-uk-icon=\"icon: phone\"></span> {{ phone.value }}</a></li>{% endif %}
            {% if email is not null %}<li><a href=\"mailto:{{ email.value }}\"><span data-uk-icon=\"icon: mail\"></span> {{ email.value }}</a></li>{% endif %}
            {% if address is not null %}<li class=\"uk-visible@s\"><a href=\"{{ 'home'|page }}#maps\" {% if mode != 'dark' %} data-uk-scroll=\"offset: 135\"{% endif %}><span data-uk-icon=\"icon: location\"></span>{{ city }}, {{ address }}</a></li>{% endif %}
        </ul>
        {% endif %}
    </div>
</nav>", "/var/www/html/grygier.local/themes/gg/partials/components/navbar-contact.htm", "");
    }
}
