<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/partials/components/navbar-menu.htm */
class __TwigTemplate_0237254b8ed612df14d664cd035aea821c9ac9e80b4f37a3eb087c12f146e487 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["socialmediaRecords"] = twig_get_attribute($this->env, $this->source, ($context["socialmedia"] ?? null), "socialMedia", [], "method", false, false, false, 1);
        // line 2
        $context["obCategoryList"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["CategoryList"] ?? null), "make", [], "method", false, false, false, 2), "tree", [], "method", false, false, false, 2);
        // line 3
        echo "
<div class=\"navbar-menu navbar-menu--";
        // line 4
        echo twig_escape_filter($this->env, ($context["mode"] ?? null), "html", null, true);
        echo "\">
    <div class=\"container-inside navbar-menu__container\">
        <nav class=\"uk-navbar-container uk-navbar-transparent\" data-uk-navbar>
            <div class=\"uk-navbar-left uk-";
        // line 7
        echo twig_escape_filter($this->env, ($context["mode"] ?? null), "html", null, true);
        echo " uk-width-expand\">
                <a href=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\" class=\"uk-navbar-item uk-logo\">
                    <img class=\"navbar-menu__logo\" src=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter((("assets/dist/images/logo--" . ($context["mode"] ?? null)) . ".png"));
        echo "\" data-src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter((("assets/dist/images/logo--" . ($context["mode"] ?? null)) . ".png"));
        echo "\" data-width=\"\" data-height=\"\" alt=\"\" data-uk-img>
                </a>

                <ul class=\"uk-navbar-nav uk-visible@m uk-width-expand uk-flex-between\">
                    ";
        // line 13
        if (twig_get_attribute($this->env, $this->source, ($context["obCategoryList"] ?? null), "isNotEmpty", [], "method", false, false, false, 13)) {
            // line 14
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["obCategoryList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["obCategory"]) {
                if ((twig_get_attribute($this->env, $this->source, $context["obCategory"], "product_count", [], "any", false, false, false, 14) > 0)) {
                    // line 15
                    echo "                    <li>
                        <a class=\"navbar-menu__link\" href=\"";
                    // line 16
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obCategory"], "getPageUrl", [0 => "shop/category"], "method", false, false, false, 16), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obCategory"], "name", [], "any", false, false, false, 16), "html", null, true);
                    echo "</a>
                        ";
                    // line 17
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["obCategory"], "children", [], "any", false, false, false, 17), "isNotEmpty", [], "method", false, false, false, 17)) {
                        // line 18
                        echo "                        <div class=\"uk-navbar-dropdown uk-padding-remove\" data-uk-dropdown=\"offset: 6; pos: bottom-left\">
                            <ul class=\"uk-nav uk-navbar-dropdown-nav navbar-submenu\">
                                ";
                        // line 20
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["obCategory"], "children", [], "any", false, false, false, 20));
                        foreach ($context['_seq'] as $context["_key"] => $context["obChildCategory"]) {
                            if ((twig_get_attribute($this->env, $this->source, $context["obChildCategory"], "product_count", [], "any", false, false, false, 20) > 0)) {
                                // line 21
                                echo "                                <li><a class=\"navbar-submenu__link\"  href=\"";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obChildCategory"], "getPageUrl", [0 => "shop/category"], "method", false, false, false, 21), "html", null, true);
                                echo "\">";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obChildCategory"], "name", [], "any", false, false, false, 21), "html", null, true);
                                echo "</a></li>
                                ";
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['obChildCategory'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 23
                        echo "                            </ul>
                        </div>
                        ";
                    }
                    // line 26
                    echo "                    </li>
                    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['obCategory'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "                    ";
        }
        // line 29
        echo "                    <li><a class=\"navbar-menu__link\" href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("shop/promotions");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Realizacje"]);
        echo "</a></li>
                    <li><a class=\"navbar-menu__link\" href=\"";
        // line 30
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "#contact\" ";
        if ((($context["mode"] ?? null) != "dark")) {
            echo "uk-scroll=\"offset: 135\" ";
        }
        echo ">";
        echo "Kontakt";
        echo "</a></li>
                </ul>
            </div>
            <div class=\"uk-navbar-right\">
                <ul class=\"uk-navbar-nav navbar-icons\">
                    <li class=\"navbar-icons__item\">
                        <a class=\"uk-button uk-light uk-button-link\" type=\"button\" data-uk-icon=\"icon: menu\"></a>
                        <div class=\"uk-navbar-dropdown uk-padding-remove\" data-uk-dropdown=\"offset: 6; pos: bottom-right\">
                            <ul class=\"uk-nav uk-navbar-dropdown-nav navbar-submenu\">
                                ";
        // line 39
        if (twig_get_attribute($this->env, $this->source, ($context["obCategoryList"] ?? null), "isNotEmpty", [], "method", false, false, false, 39)) {
            // line 40
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["obCategoryList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["obCategory"]) {
                if ((twig_get_attribute($this->env, $this->source, $context["obCategory"], "product_count", [], "any", false, false, false, 40) > 0)) {
                    // line 41
                    echo "                                    <li class=\"uk-hidden@m\"><a class=\"navbar-submenu__link\" href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obCategory"], "getPageUrl", [0 => "shop/category"], "method", false, false, false, 41), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obCategory"], "name", [], "any", false, false, false, 41), "html", null, true);
                    echo "</a></li>
                                ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['obCategory'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "                                ";
        }
        // line 44
        echo "                                <li><a class=\"navbar-submenu__link\" href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/category", ["slug" => "inspiracje"]);
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Inspiracje"]);
        echo "</a></li>
                                <li><a class=\"navbar-submenu__link\" href=\"";
        // line 45
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/category", ["slug" => "realizacje"]);
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Realizacje"]);
        echo "</a></li>
                                <li><a class=\"navbar-submenu__link\" href=\"";
        // line 46
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("documents");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Dokumenty"]);
        echo "</a></li>
                                <li><a class=\"navbar-submenu__link\" href=\"";
        // line 47
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("faq");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["FAQ"]);
        echo "</a></li>
                                <li class=\"uk-hidden@m\"><a class=\"navbar-submenu__link\" href=\"";
        // line 48
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("shop/promotions");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Promocje"]);
        echo "</a></li>
                                <li class=\"uk-hidden@m\"><a class=\"navbar-submenu__link\" href=\"";
        // line 49
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "#contact\" ";
        if ((($context["mode"] ?? null) != "dark")) {
            echo "uk-scroll=\"offset: 135\" ";
        }
        echo ">";
        echo "Kontakt";
        echo "</a></li>
                                <li>
                                    <div class=\"uk-flex uk-flex-center menu-social uk-padding-small\">
                                        ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["socialmediaRecords"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["social"]) {
            // line 53
            echo "                                        <a href=\"";
            echo twig_escape_filter($this->env, $context["social"], "html", null, true);
            echo "\" class=\"menu-social__item uk-margin-right\" data-uk-icon=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\"></a>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['social'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class=\"navbar-icons__item\">
                        <a class=\"uk-button uk-light uk-button-link\" type=\"button\" data-uk-icon=\"icon: search\"></a>
                        <div class=\"uk-navbar-dropdown navbar-search\" data-uk-dropdown=\"offset: 6; pos: bottom-right\">
                            <form action=\"";
        // line 63
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("shop/search");
        echo "\" class=\"uk-search uk-search-default\">
                                <span data-uk-search-icon></span>
                                <input class=\"uk-search-input navbar-search__input\" name=\"search\" type=\"search\" placeholder=\"";
        // line 65
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Szukaj..."]);
        echo "\">
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/partials/components/navbar-menu.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 65,  239 => 63,  229 => 55,  218 => 53,  214 => 52,  202 => 49,  196 => 48,  190 => 47,  184 => 46,  178 => 45,  171 => 44,  168 => 43,  156 => 41,  150 => 40,  148 => 39,  130 => 30,  123 => 29,  120 => 28,  112 => 26,  107 => 23,  95 => 21,  90 => 20,  86 => 18,  84 => 17,  78 => 16,  75 => 15,  69 => 14,  67 => 13,  58 => 9,  54 => 8,  50 => 7,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set socialmediaRecords = socialmedia.socialMedia() %}
{% set obCategoryList = CategoryList.make().tree() %}

<div class=\"navbar-menu navbar-menu--{{ mode }}\">
    <div class=\"container-inside navbar-menu__container\">
        <nav class=\"uk-navbar-container uk-navbar-transparent\" data-uk-navbar>
            <div class=\"uk-navbar-left uk-{{ mode }} uk-width-expand\">
                <a href=\"{{ 'home'|page }}\" class=\"uk-navbar-item uk-logo\">
                    <img class=\"navbar-menu__logo\" src=\"{{ ('assets/dist/images/logo--' ~ mode ~ '.png')|theme }}\" data-src=\"{{ ('assets/dist/images/logo--' ~ mode ~ '.png')|theme }}\" data-width=\"\" data-height=\"\" alt=\"\" data-uk-img>
                </a>

                <ul class=\"uk-navbar-nav uk-visible@m uk-width-expand uk-flex-between\">
                    {% if obCategoryList.isNotEmpty() %}
                    {% for obCategory in obCategoryList if obCategory.product_count > 0 %}
                    <li>
                        <a class=\"navbar-menu__link\" href=\"{{ obCategory.getPageUrl('shop/category') }}\">{{ obCategory.name }}</a>
                        {% if obCategory.children.isNotEmpty() %}
                        <div class=\"uk-navbar-dropdown uk-padding-remove\" data-uk-dropdown=\"offset: 6; pos: bottom-left\">
                            <ul class=\"uk-nav uk-navbar-dropdown-nav navbar-submenu\">
                                {% for obChildCategory in obCategory.children if obChildCategory.product_count > 0 %}
                                <li><a class=\"navbar-submenu__link\"  href=\"{{ obChildCategory.getPageUrl('shop/category') }}\">{{ obChildCategory.name }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                        {% endif %}
                    </li>
                    {% endfor %}
                    {% endif %}
                    <li><a class=\"navbar-menu__link\" href=\"{{ 'shop/promotions'|page }}\">{{ 'Realizacje'|_ }}</a></li>
                    <li><a class=\"navbar-menu__link\" href=\"{{ 'home'|page }}#contact\" {% if mode != 'dark' %}uk-scroll=\"offset: 135\" {% endif %}>{{ 'Kontakt' }}</a></li>
                </ul>
            </div>
            <div class=\"uk-navbar-right\">
                <ul class=\"uk-navbar-nav navbar-icons\">
                    <li class=\"navbar-icons__item\">
                        <a class=\"uk-button uk-light uk-button-link\" type=\"button\" data-uk-icon=\"icon: menu\"></a>
                        <div class=\"uk-navbar-dropdown uk-padding-remove\" data-uk-dropdown=\"offset: 6; pos: bottom-right\">
                            <ul class=\"uk-nav uk-navbar-dropdown-nav navbar-submenu\">
                                {% if obCategoryList.isNotEmpty() %}
                                {% for obCategory in obCategoryList if obCategory.product_count > 0 %}
                                    <li class=\"uk-hidden@m\"><a class=\"navbar-submenu__link\" href=\"{{ obCategory.getPageUrl('shop/category') }}\">{{ obCategory.name }}</a></li>
                                {% endfor %}
                                {% endif %}
                                <li><a class=\"navbar-submenu__link\" href=\"{{ 'blog/category'|page({slug: 'inspiracje'}) }}\">{{ 'Inspiracje'|_ }}</a></li>
                                <li><a class=\"navbar-submenu__link\" href=\"{{ 'blog/category'|page({slug: 'realizacje'}) }}\">{{ 'Realizacje'|_ }}</a></li>
                                <li><a class=\"navbar-submenu__link\" href=\"{{ 'documents'|page }}\">{{ 'Dokumenty'|_ }}</a></li>
                                <li><a class=\"navbar-submenu__link\" href=\"{{ 'faq'|page }}\">{{ 'FAQ'|_ }}</a></li>
                                <li class=\"uk-hidden@m\"><a class=\"navbar-submenu__link\" href=\"{{ 'shop/promotions'|page }}\">{{ 'Promocje'|_ }}</a></li>
                                <li class=\"uk-hidden@m\"><a class=\"navbar-submenu__link\" href=\"{{ 'home'|page }}#contact\" {% if mode != 'dark' %}uk-scroll=\"offset: 135\" {% endif %}>{{ 'Kontakt' }}</a></li>
                                <li>
                                    <div class=\"uk-flex uk-flex-center menu-social uk-padding-small\">
                                        {% for key, social in socialmediaRecords %}
                                        <a href=\"{{ social }}\" class=\"menu-social__item uk-margin-right\" data-uk-icon=\"{{ key }}\"></a>
                                        {% endfor %}
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class=\"navbar-icons__item\">
                        <a class=\"uk-button uk-light uk-button-link\" type=\"button\" data-uk-icon=\"icon: search\"></a>
                        <div class=\"uk-navbar-dropdown navbar-search\" data-uk-dropdown=\"offset: 6; pos: bottom-right\">
                            <form action=\"{{ 'shop/search'|page }}\" class=\"uk-search uk-search-default\">
                                <span data-uk-search-icon></span>
                                <input class=\"uk-search-input navbar-search__input\" name=\"search\" type=\"search\" placeholder=\"{{ 'Szukaj...'|_ }}\">
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>", "/var/www/html/grygier.local/themes/gg/partials/components/navbar-menu.htm", "");
    }
}
