<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/plugins/grape/media/components/imageresponsive/picture.htm */
class __TwigTemplate_94cf7b93aa39957277e526e22d61e1a6e04235d3af48803e2f9704cb6457c3ec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["crop"] = (((null === ($context["crop"] ?? null))) ? (["width" => "1600", "height" => "auto", "mode" => "auto"]) : (($context["crop"] ?? null)));
        // line 2
        echo "<picture ";
        echo ($context["attrPicture"] ?? null);
        echo ">
    ";
        // line 3
        if (twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "small", [], "any", false, false, false, 3)) {
            echo "<source media=\"(max-width: 639px)\" srcset=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "small", [], "any", false, false, false, 3), "thumb", [0 => 639, 1 => ($context["auto"] ?? null)], "method", false, false, false, 3), "html", null, true);
            echo "\">";
        }
        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "medium", [], "any", false, false, false, 4)) {
            echo "<source media=\"(max-width: 959px)\" srcset=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "medium", [], "any", false, false, false, 4), "thumb", [0 => 959, 1 => ($context["auto"] ?? null)], "method", false, false, false, 4), "html", null, true);
            echo "\">";
        }
        // line 5
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "large", [], "any", false, false, false, 5)) {
            echo "<source media=\"(max-width: 1199px)\" srcset=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "large", [], "any", false, false, false, 5), "thumb", [0 => 1199, 1 => ($context["auto"] ?? null)], "method", false, false, false, 5), "html", null, true);
            echo "\">";
        }
        // line 6
        echo "    <img src=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "xlarge", [], "any", false, false, false, 6), "thumb", [0 => twig_get_attribute($this->env, $this->source, ($context["crop"] ?? null), "width", [], "any", false, false, false, 6), 1 => twig_get_attribute($this->env, $this->source, ($context["crop"] ?? null), "height", [], "any", false, false, false, 6), 2 => twig_get_attribute($this->env, $this->source, ($context["crop"] ?? null), "mode", [], "any", false, false, false, 6)], "method", false, false, false, 6), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "xlarge", [], "any", false, false, false, 6), "title", [], "any", false, false, false, 6), "html", null, true);
        echo "\" ";
        echo ($context["attrImg"] ?? null);
        echo ">
</picture>
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/plugins/grape/media/components/imageresponsive/picture.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 6,  57 => 5,  50 => 4,  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set crop = (crop is null)? { 'width': '1600', 'height': 'auto', 'mode': 'auto' }: crop %}
<picture {{ attrPicture|raw }}>
    {% if image.small %}<source media=\"(max-width: 639px)\" srcset=\"{{ image.small.thumb(639,auto) }}\">{% endif %}
    {% if image.medium %}<source media=\"(max-width: 959px)\" srcset=\"{{ image.medium.thumb(959,auto) }}\">{% endif %}
    {% if image.large %}<source media=\"(max-width: 1199px)\" srcset=\"{{ image.large.thumb(1199,auto) }}\">{% endif %}
    <img src=\"{{ image.xlarge.thumb(crop.width, crop.height, crop.mode) }}\" alt=\"{{ image.xlarge.title }}\" {{ attrImg|raw }}>
</picture>
", "/var/www/html/grygier.local/plugins/grape/media/components/imageresponsive/picture.htm", "");
    }
}
