<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/partials/layouts/default/header.htm */
class __TwigTemplate_2791671943324a3f94e5067a4775f8c57444347137e3c5bdae0f36da584be195 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header class=\"header\" id=\"layout-header\">
    <div class=\"header-navbar\" data-uk-sticky>
        ";
        // line 3
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("components/navbar-contact"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 4
        echo "        ";
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['mode'] = "light"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("components/navbar-menu"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 5
        echo "    </div>
    ";
        // line 6
        if (twig_get_attribute($this->env, $this->source, ($context["slider"] ?? null), "slider", [], "method", false, false, false, 6)) {
            // line 7
            echo "    <div class=\"slider-home uk-position-relative uk-visible-toggle uk-light\" tabindex=\"-1\" data-uk-slideshow=\"animation: push; autoplay: false\">
        <ul class=\"uk-slideshow-items\">
            ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["slider"] ?? null), "slider", [], "method", false, false, false, 9), "sliderItems", [], "any", false, false, false, 9));
            foreach ($context['_seq'] as $context["_key"] => $context["slide"]) {
                // line 10
                echo "            ";
                if (twig_get_attribute($this->env, $this->source, $context["slide"], "poster", [], "any", false, false, false, 10)) {
                    // line 11
                    echo "            <li>
                <div class=\"uk-position-cover\">
                    ";
                    // line 13
                    echo twig_get_attribute($this->env, $this->source, ($context["imageresponsive"] ?? null), "picture", [0 => twig_get_attribute($this->env, $this->source, $context["slide"], "poster", [], "any", false, false, false, 13), 1 => null, 2 => "uk-cover", 3 => " style=\"width: 100%\""], "method", false, false, false, 13);
                    echo "
                </div>
                <div class=\"slider-home__layer uk-position-bottom-right\">
                    ";
                    // line 16
                    $context['__cms_partial_params'] = [];
                    $context['__cms_partial_params']['slide'] = $context["slide"]                    ;
                    echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("slider/layer"                    , $context['__cms_partial_params']                    , true                    );
                    unset($context['__cms_partial_params']);
                    // line 17
                    echo "                </div>

                ";
                    // line 19
                    if (twig_get_attribute($this->env, $this->source, $context["slide"], "content", [], "any", false, false, false, 19)) {
                        // line 20
                        echo "                ";
                        $context["classes"] = ["small" => "uk-hidden@s", "medium" => "uk-hidden@m  uk-visible@s", "large" => "uk-hidden@l uk-visible@m", "xlarge" => "uk-visible@l"];
                        // line 26
                        echo "                <div class=\"uk-position-bottom-right\">
                    ";
                        // line 27
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["slide"], "content", [], "any", false, false, false, 27));
                        foreach ($context['_seq'] as $context["_key"] => $context["content"]) {
                            // line 28
                            echo "                    <div class=\"slider-home__text ";
                            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["classes"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[twig_get_attribute($this->env, $this->source, $context["content"], "size", [], "any", false, false, false, 28)] ?? null) : null), "html", null, true);
                            echo "\" data-uk-slideshow-parallax=\"scale: 1,1,0.8\">
                        ";
                            // line 29
                            echo twig_get_attribute($this->env, $this->source, $context["content"], "content", [], "any", false, false, false, 29);
                            echo "
                    </div>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['content'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 32
                        echo "                </div>
                ";
                    }
                    // line 34
                    echo "            </li>
            ";
                }
                // line 36
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slide'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "        </ul>
        <a class=\"uk-position-center-left uk-position-small uk-hidden-hover\" href=\"#\" data-uk-slidenav-previous data-uk-slideshow-item=\"previous\"></a>
        <a class=\"uk-position-center-right uk-position-small uk-hidden-hover\" href=\"#\" data-uk-slidenav-next data-uk-slideshow-item=\"next\"></a>
    </div>
    ";
        }
        // line 42
        echo "</header>
<hr class=\"header-bottom-line\">";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/partials/layouts/default/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 42,  126 => 37,  120 => 36,  116 => 34,  112 => 32,  103 => 29,  98 => 28,  94 => 27,  91 => 26,  88 => 20,  86 => 19,  82 => 17,  77 => 16,  71 => 13,  67 => 11,  64 => 10,  60 => 9,  56 => 7,  54 => 6,  51 => 5,  45 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header class=\"header\" id=\"layout-header\">
    <div class=\"header-navbar\" data-uk-sticky>
        {% partial 'components/navbar-contact'%}
        {% partial 'components/navbar-menu' mode=\"light\" %}
    </div>
    {% if slider.slider() %}
    <div class=\"slider-home uk-position-relative uk-visible-toggle uk-light\" tabindex=\"-1\" data-uk-slideshow=\"animation: push; autoplay: false\">
        <ul class=\"uk-slideshow-items\">
            {% for slide in slider.slider().sliderItems %}
            {% if slide.poster %}
            <li>
                <div class=\"uk-position-cover\">
                    {{ imageresponsive.picture(slide.poster, null, 'uk-cover', ' style=\"width: 100%\"')|raw }}
                </div>
                <div class=\"slider-home__layer uk-position-bottom-right\">
                    {% partial 'slider/layer' slide=slide %}
                </div>

                {% if slide.content %}
                {% set classes = {
                    'small': 'uk-hidden@s',
                    'medium': 'uk-hidden@m  uk-visible@s',
                    'large': 'uk-hidden@l uk-visible@m',
                    'xlarge': 'uk-visible@l',
                } %}
                <div class=\"uk-position-bottom-right\">
                    {% for content in slide.content %}
                    <div class=\"slider-home__text {{ classes[content.size] }}\" data-uk-slideshow-parallax=\"scale: 1,1,0.8\">
                        {{ content.content|raw }}
                    </div>
                    {% endfor %}
                </div>
                {% endif %}
            </li>
            {% endif %}
            {% endfor %}
        </ul>
        <a class=\"uk-position-center-left uk-position-small uk-hidden-hover\" href=\"#\" data-uk-slidenav-previous data-uk-slideshow-item=\"previous\"></a>
        <a class=\"uk-position-center-right uk-position-small uk-hidden-hover\" href=\"#\" data-uk-slidenav-next data-uk-slideshow-item=\"next\"></a>
    </div>
    {% endif %}
</header>
<hr class=\"header-bottom-line\">", "/var/www/html/grygier.local/themes/gg/partials/layouts/default/header.htm", "");
    }
}
