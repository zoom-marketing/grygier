<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/plugins/grape/additionalcode/components/additionalcode/default.htm */
class __TwigTemplate_bc58dddf7f3e46a15562deddc868d61c9624f80ca86e1c07c96b994d3d4e1fd0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('scripts'        );
        // line 2
        echo ($context["scripts"] ?? null);
        echo "
";
        // line 1
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 4
        echo "
";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('styles'        );
        // line 6
        echo ($context["styles"] ?? null);
        echo "
";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 8
        echo "
";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('headAfter'        );
        // line 10
        echo ($context["headAfter"] ?? null);
        echo "
";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 12
        echo "
";
        // line 13
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('bodyBefore'        );
        // line 14
        echo ($context["bodyBefore"] ?? null);
        echo "
";
        // line 13
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 16
        echo "
";
        // line 17
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('bodyAfter'        );
        // line 18
        echo ($context["bodyAfter"] ?? null);
        echo "
";
        // line 17
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 20
        echo "
";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('appIcons'        );
        // line 22
        echo "<!--Favicon && App Icons -->
";
        // line 23
        if (($context["appIcon"] ?? null)) {
            // line 24
            echo "<link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 57, 1 => 57, 2 => "crop"], "method", false, false, false, 24), "html", null, true);
            echo "\">
<link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 60, 1 => 60, 2 => "crop"], "method", false, false, false, 25), "html", null, true);
            echo "\">
<link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 72, 1 => 72, 2 => "crop"], "method", false, false, false, 26), "html", null, true);
            echo "\">
<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 76, 1 => 76, 2 => "crop"], "method", false, false, false, 27), "html", null, true);
            echo "\">
<link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 114, 1 => 114, 2 => "crop"], "method", false, false, false, 28), "html", null, true);
            echo "\">
<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 120, 1 => 120, 2 => "crop"], "method", false, false, false, 29), "html", null, true);
            echo "\">
<link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 144, 1 => 144, 2 => "crop"], "method", false, false, false, 30), "html", null, true);
            echo "\">
<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 152, 1 => 152, 2 => "crop"], "method", false, false, false, 31), "html", null, true);
            echo "\">
<link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"";
            // line 32
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 180, 1 => 180, 2 => "crop"], "method", false, false, false, 32), "html", null, true);
            echo "\">
<link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 192, 1 => 192, 2 => "crop"], "method", false, false, false, 33), "html", null, true);
            echo "\">
<link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 32, 1 => 32, 2 => "crop"], "method", false, false, false, 34), "html", null, true);
            echo "\">
<link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 96, 1 => 96, 2 => "crop"], "method", false, false, false, 35), "html", null, true);
            echo "\">
<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appIcon"] ?? null), "thumb", [0 => 16, 1 => 16, 2 => "crop"], "method", false, false, false, 36), "html", null, true);
            echo "\">
<meta name=\"msapplication-TileImage\" content=\"";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["appicon"] ?? null), "thumb", [0 => 144, 1 => 144, 2 => "crop"], "method", false, false, false, 37), "html", null, true);
            echo "\">
";
            // line 38
            if (($context["appColor"] ?? null)) {
                // line 39
                echo "<meta name=\"msapplication-TileColor\" content=\"";
                echo twig_escape_filter($this->env, ($context["appColor"] ?? null), "html", null, true);
                echo "\">
<meta name=\"theme-color\" content=\"";
                // line 40
                echo twig_escape_filter($this->env, ($context["appColor"] ?? null), "html", null, true);
                echo "\">
";
            }
        }
        // line 43
        echo "<!--Favicon && App Icons -->
";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 45
        echo "

";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/plugins/grape/additionalcode/components/additionalcode/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 45,  172 => 21,  169 => 43,  163 => 40,  158 => 39,  156 => 38,  152 => 37,  148 => 36,  144 => 35,  140 => 34,  136 => 33,  132 => 32,  128 => 31,  124 => 30,  120 => 29,  116 => 28,  112 => 27,  108 => 26,  104 => 25,  99 => 24,  97 => 23,  94 => 22,  92 => 21,  89 => 20,  87 => 17,  83 => 18,  81 => 17,  78 => 16,  76 => 13,  72 => 14,  70 => 13,  67 => 12,  65 => 9,  61 => 10,  59 => 9,  56 => 8,  54 => 5,  50 => 6,  48 => 5,  45 => 4,  43 => 1,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% put scripts %}
{{ scripts|raw }}
{% endput %}

{% put styles %}
{{ styles|raw }}
{% endput %}

{% put headAfter %}
{{ headAfter|raw }}
{% endput %}

{% put bodyBefore %}
{{ bodyBefore|raw }}
{% endput %}

{% put bodyAfter %}
{{ bodyAfter|raw }}
{% endput %}

{% put appIcons %}
<!--Favicon && App Icons -->
{% if appIcon %}
<link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"{{ appIcon.thumb(57,57,'crop') }}\">
<link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"{{ appIcon.thumb(60,60,'crop') }}\">
<link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"{{ appIcon.thumb(72,72,'crop') }}\">
<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"{{ appIcon.thumb(76,76,'crop') }}\">
<link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"{{ appIcon.thumb(114,114,'crop') }}\">
<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"{{ appIcon.thumb(120,120,'crop') }}\">
<link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"{{ appIcon.thumb(144,144,'crop') }}\">
<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"{{ appIcon.thumb(152,152,'crop') }}\">
<link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"{{ appIcon.thumb(180,180,'crop') }}\">
<link rel=\"icon\" type=\"image/png\" sizes=\"192x192\"  href=\"{{ appIcon.thumb(192,192,'crop') }}\">
<link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"{{ appIcon.thumb(32,32,'crop') }}\">
<link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"{{ appIcon.thumb(96,96,'crop') }}\">
<link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"{{ appIcon.thumb(16,16,'crop') }}\">
<meta name=\"msapplication-TileImage\" content=\"{{ appicon.thumb(144,144,'crop') }}\">
{% if appColor %}
<meta name=\"msapplication-TileColor\" content=\"{{ appColor }}\">
<meta name=\"theme-color\" content=\"{{ appColor }}\">
{% endif %}
{% endif %}
<!--Favicon && App Icons -->
{% endput %}


", "/var/www/html/grygier.local/plugins/grape/additionalcode/components/additionalcode/default.htm", "");
    }
}
