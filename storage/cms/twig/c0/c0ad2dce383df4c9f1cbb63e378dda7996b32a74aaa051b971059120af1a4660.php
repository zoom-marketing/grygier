<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/pages/home.htm */
class __TwigTemplate_f0b3a263e24519113ce184c9e63325b83183f9673c9a80fef0d59ac634a1c7bb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["page"] = twig_get_attribute($this->env, $this->source, ($context["pages"] ?? null), "page", [0 => "home"], "method", false, false, false, 1);
        // line 2
        echo twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "seo", [0 => twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "seo", [], "any", false, false, false, 2)], "method", false, false, false, 2);
        echo "

";
        // line 4
        $context["postsRecords"] = twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "paginate", [0 => 10], "method", false, false, false, 4);
        // line 5
        $context["postsRecords"] = twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "category", [0 => "realizacje", 1 => true, 2 => 20], "method", false, false, false, 5);
        // line 6
        $context["iconsboxRecords"] = twig_get_attribute($this->env, $this->source, ($context["iconsbox"] ?? null), "box", [0 => "home"], "method", false, false, false, 6);
        // line 7
        $context["testimonialGroup"] = twig_get_attribute($this->env, $this->source, ($context["testimonial"] ?? null), "testimonialGroup", [0 => "home"], "method", false, false, false, 7);
        // line 8
        $context["socialmediaRecords"] = twig_get_attribute($this->env, $this->source, ($context["socialmedia"] ?? null), "socialMedia", [], "method", false, false, false, 8);
        // line 9
        echo "
";
        // line 10
        $context["place"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "place", [0 => "primary"], "method", false, false, false, 10);
        // line 11
        if (($context["place"] ?? null)) {
            // line 12
            $context["companyName"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "name"], "method", false, false, false, 12);
            // line 13
            $context["phone"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "phones", 2 => "primary"], "method", false, false, false, 13);
            // line 14
            $context["email"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "emails", 2 => "primary"], "method", false, false, false, 14);
            // line 15
            $context["hours"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "hours"], "method", false, false, false, 15);
            // line 16
            $context["address"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "address"], "method", false, false, false, 16);
            // line 17
            $context["postcode"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "post_code"], "method", false, false, false, 17);
            // line 18
            $context["city"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "city"], "method", false, false, false, 18);
            // line 19
            $context["taxNumber"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "tax_number"], "method", false, false, false, 19);
            // line 20
            $context["registrationNumber"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "registration_number"], "method", false, false, false, 20);
            // line 21
            echo "
";
        }
        // line 23
        echo "
<section class=\"uk-section uk-section-default\">
    <div class=\"container-inside uk-margin-auto uk-flex uk-grid-collapse\" uk-grid>
        <div class=\"uk-width-3-5@m uk-flex uk-flex-column uk-flex-center\">
            <div class=\"history uk-margin-auto\">
                <div class=\"history__top\">
                    <span class=\"history__top__prefix\">Od</span>
                    <span class=\"history__top__data\" data-uk-scrollspy=\"cls:uk-animation-slide-top\">44</span>
                    <span class=\"history__top__suffix\">lat.</span>
                </div>
                <div class=\"history__description\">
                    ";
        // line 34
        echo twig_get_attribute($this->env, $this->source, ($context["pages"] ?? null), "extendedField", [0 => "home", 1 => "about"], "method", false, false, false, 34);
        echo "
                </div>
            </div>
        </div>
        <div class=\"uk-width-2-5@m uk-visible@m uk-text-center\">
            <img class=\"uk-animation-scale-up\" style=\"max-width:250px;\"src=\"";
        // line 39
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/dist/images/history.png");
        echo "\" data-src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/dist/images/history.png");
        echo "\" data-width=\"\" data-height=\"\" alt=\"\" data-uk-img>
        </div>
        <div class=\"uk-width-1-1 uk-child-width-1-1 uk-child-width-1-3@m uk-flex uk-flex-wrap uk-margin-medium-top\">
            <div class=\"history-icon\">
                <span class=\"history-icon__prefix\">";
        // line 43
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Niemal"]);
        echo "</span>
                <span class=\"history-icon__data\" data-uk-scrollspy=\"cls:uk-animation-slide-top\">";
        // line 44
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["pół"]);
        echo "</span>
                <span class=\"history-icon__suffix\">";
        // line 45
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["wieku"]);
        echo "</span>
                <div class=\"history-icon__description\">";
        // line 46
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["doświadczenia"]);
        echo "</div>
            </div>
            <div class=\"history-icon\">
                <span class=\"history-icon__prefix\">";
        // line 49
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Ponad"]);
        echo "</span>
                <span class=\"history-icon__data\" data-uk-scrollspy=\"cls:uk-animation-slide-top\">";
        // line 50
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["15"]);
        echo "</span>
                <span class=\"history-icon__suffix\">";
        // line 51
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["tysięcy"]);
        echo "</span>
                <div class=\"history-icon__description\">";
        // line 52
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["realizacji"]);
        echo "</div>
            </div>
            <div class=\"history-icon\">
                <span class=\"history-icon__prefix\">";
        // line 55
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Park"]);
        echo "</span>
                <span class=\"history-icon__data\" data-uk-scrollspy=\"cls:uk-animation-slide-top\">";
        // line 56
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["maszyn"]);
        echo "</span>
                <div class=\"history-icon__description\">";
        // line 57
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["najwyższej klasy"]);
        echo "</div>
            </div>
        </div>
    </div>
</section>
";
        // line 62
        if ((twig_length_filter($this->env, ($context["postsRecords"] ?? null)) > 0)) {
            // line 63
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['title'] = "Realizacje"            ;
            $context['__cms_partial_params']['subtitle'] = "Nasze"            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("components/bar"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 64
            echo "<section class=\"uk-section uk-section-default\">
    <div class=\"container-inside uk-margin-auto uk-flex uk-grid-collapse\" data-uk-grid>
        <div class=\"uk-width-1-1 uk-flex uk-flex-wrap uk-flex-wrap-reverse\">
            <div class=\"blog-list__more uk-margin\">
                <a  class=\"blog-list__more__link\" href=\"";
            // line 68
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/category", ["slug" => "realizacje"]);
            echo "\">";
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Zobacz więcej"]);
            echo "</a>
            </div>
            ";
            // line 70
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, ($context["postsRecords"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                // line 71
                echo "            ";
                if (twig_get_attribute($this->env, $this->source, $context["post"], "is_home", [], "any", false, false, false, 71)) {
                    // line 72
                    echo "            <section class=\"uk-width-1-2 uk-width-1-3@m \">
                <a href=\"";
                    // line 73
                    echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/post", ["slug" => twig_get_attribute($this->env, $this->source, $context["post"], "slug", [], "any", false, false, false, 73)]);
                    echo "\">
                    ";
                    // line 74
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "poster", [], "any", false, false, false, 74), "xlarge", [], "any", false, false, false, 74)) {
                        // line 75
                        echo "                        <img data-uk-scrollspy=\"cls:uk-animation-scale-up\" data-src=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "poster", [], "any", false, false, false, 75), "xlarge", [], "any", false, false, false, 75), "thumb", [0 => 400, 1 => 400, 2 => "crop"], "method", false, false, false, 75), "html", null, true);
                        echo "\" data-width=\"\" data-height=\"\" alt=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 75), "html", null, true);
                        echo "\" data-uk-img>
                    ";
                    } else {
                        // line 77
                        echo "                        <img data-uk-scrollspy=\"cls:uk-animation-scale-up\" data-src=\"";
                        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/dist/images/posts-listing--home.jpg");
                        echo "\" data-width=\"\" data-height=\"\" alt=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, false, 77), "html", null, true);
                        echo "\" data-uk-img>
                    ";
                    }
                    // line 79
                    echo "                </a>
            </section>
            ";
                }
                // line 82
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "            <section class=\"uk-width-1-1 uk-width-2-3@m blog-list__header\">
                <h1>";
            // line 84
            echo twig_get_attribute($this->env, $this->source, ($context["pages"] ?? null), "extendedField", [0 => "home", 1 => "realizations_title"], "method", false, false, false, 84);
            echo "</h1>
                <hr>
                <div>
                    ";
            // line 87
            echo twig_get_attribute($this->env, $this->source, ($context["pages"] ?? null), "extendedField", [0 => "home", 1 => "realizations_description"], "method", false, false, false, 87);
            echo "
                </div>
            </section>
        </div>
    </div>
</section>
";
        }
        // line 94
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['title'] = "Otrzymasz:"        ;
        $context['__cms_partial_params']['subtitle'] = "Wybierając nas"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("components/bar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 95
        if (($context["iconsboxRecords"] ?? null)) {
            // line 96
            echo "<section class=\"uk-section uk-section-default\">
    <div class=\"container-inside uk-margin-auto uk-flex uk-grid-collapse\" data-uk-grid>
        <div class=\"uk-width-1-1 uk-child-width-1-1 uk-child-width-1-3@m uk-flex uk-flex-wrap\">
            ";
            // line 99
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["iconsboxRecords"] ?? null), "icons", [], "any", false, false, false, 99));
            foreach ($context['_seq'] as $context["_key"] => $context["box"]) {
                // line 100
                echo "            <div class=\"uk-flex uk-flex-center\">
                <section class=\"icons-box uk-padding uk-margin-auto-left uk-margin-auto-right\">
                    ";
                // line 102
                if (twig_get_attribute($this->env, $this->source, $context["box"], "icon", [], "any", false, false, false, 102)) {
                    // line 103
                    echo "                        <img data-uk-scrollspy=\"cls:uk-animation-scale-up\" src=\"";
                    echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["box"], "icon", [], "any", false, false, false, 103));
                    echo "\" data-src=\"";
                    echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["box"], "icon", [], "any", false, false, false, 103));
                    echo "\" data-width=\"\" data-height=\"\" alt=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["box"], "name", [], "any", false, false, false, 103), "html", null, true);
                    echo "\" data-uk-img>
                    ";
                }
                // line 105
                echo "                    <h2 class=\"icons-box__title\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["box"], "name", [], "any", false, false, false, 105), "html", null, true);
                echo "</h2>
                    <hr class=\"icons-box__pipe\">
                    <p class=\"icons-box__description\">
                        ";
                // line 108
                echo twig_get_attribute($this->env, $this->source, $context["box"], "description", [], "any", false, false, false, 108);
                echo "
                    </p>
                </section>
            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['box'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 113
            echo "        </div>
    </div>
</section>
";
        }
        // line 117
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['title'] = "O nas:"        ;
        $context['__cms_partial_params']['subtitle'] = "Kilka słów"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("components/bar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 118
        echo "<section class=\"uk-section uk-section-default\" id=\"about\">
    <div class=\"container-inside uk-margin-auto uk-position-relative\" data-uk-slider=\"autoplay: false; sets: true\">
        <ul class=\"uk-slider-items uk-child-width-1-1@s uk-child-width-1-3@m\">
            ";
        // line 121
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["testimonialGroup"] ?? null), "testimonials", [], "any", false, false, false, 121));
        foreach ($context['_seq'] as $context["_key"] => $context["testimonial"]) {
            // line 122
            echo "            <li class=\"uk-flex uk-flex-center\">
                <div class=\"icons-box uk-padding\">
                    ";
            // line 124
            if (twig_get_attribute($this->env, $this->source, $context["testimonial"], "poster", [], "any", false, false, false, 124)) {
                // line 125
                echo "                        <img class=\"icons-box__icon\" src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["testimonial"], "poster", [], "any", false, false, false, 125), "thumb", [0 => 128, 1 => 128, 2 => "crop"], "method", false, false, false, 125), "html", null, true);
                echo "\" data-src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["testimonial"], "poster", [], "any", false, false, false, 125), "thumb", [0 => 128, 1 => 128, 2 => "crop"], "method", false, false, false, 125), "html", null, true);
                echo "\" data-width=\"\" data-height=\"\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, false, 125), "html", null, true);
                echo "\" data-uk-img>
                    ";
            }
            // line 127
            echo "                    <h2 class=\"icons-box__title\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["testimonial"], "name", [], "any", false, false, false, 127), "html", null, true);
            echo "</h2>
                    <hr class=\"icons-box__pipe\">
                    <p class=\"icons-box__description\">
                        ";
            // line 130
            echo twig_get_attribute($this->env, $this->source, $context["testimonial"], "description", [], "any", false, false, false, 130);
            echo "
                    </p>
                </div>
            </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['testimonial'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 135
        echo "        </ul>
    </div>
</section>
";
        // line 138
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['title'] = "Kontaktu:"        ;
        $context['__cms_partial_params']['subtitle'] = "Zapraszamy do"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("components/bar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 139
        echo "<section class=\"uk-section uk-section-default\" id=\"contact\">
    <div class=\"container-inside uk-margin-auto uk-width-1-1 uk-child-width-1-1 uk-child-width-1-2@m uk-flex uk-flex-center uk-flex-middle uk-flex-wrap contact\">
        <div class=\"contact-form contact-form--left\">
            <div class=\"contact-form__description\">
                ";
        // line 143
        echo twig_get_attribute($this->env, $this->source, ($context["pages"] ?? null), "extendedField", [0 => "home", 1 => "contact_description"], "method", false, false, false, 143);
        echo "
            </div>
            <div class=\"contact-form__form\">
                <form data-request=\"contact::onSubmit\" data-request-files class=\"uk-grid-small\"  data-uk-grid>
                    <div class=\"uk-width-1-2@s\">
                        <label class=\"uk-form-label\">";
        // line 148
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Imię"]);
        echo "</label>
                        <input class=\"uk-input\" name=\"name\" type=\"text\" placeholder=\"";
        // line 149
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Imię"]);
        echo "\">
                    </div>
                    <div class=\"uk-width-1-2@s\">
                        <label class=\"uk-form-label\">";
        // line 152
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Telefon"]);
        echo "</label>
                        <input class=\"uk-input\" name=\"phone\" type=\"tel\" placeholder=\"";
        // line 153
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Telefon"]);
        echo "\">
                    </div>
                    <div class=\"uk-width-1-1@s\">
                        <label class=\"uk-form-label\">";
        // line 156
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Adres email"]);
        echo "</label>
                        <input class=\"uk-input\" name=\"email\" type=\"email\" placeholder=\"";
        // line 157
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Adres email"]);
        echo "\">
                    </div>
                    <div class=\"uk-width-1-1\">
                        <label class=\"uk-form-label\">";
        // line 160
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Wiadomość"]);
        echo "</label>
                        <textarea class=\"uk-textarea\" rows=\"5\" name=\"message\" placeholder=\"";
        // line 161
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Wiadomość"]);
        echo "\"></textarea>
                    </div>
                    <div class=\"uk-width-1-1\" id=\"js-upload-container\">
                        <div>
                            <label class=\"uk-form-label\">";
        // line 165
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Załącz pliki"]);
        echo "</label>
                        </div>
                        <div>
                            <div class=\"uk-margin-small\" data-uk-form-custom=\"target: true\">
                                <a class=\"uk-form-icon uk-form-icon-flip\" href=\"\" data-uk-icon=\"icon: download\"></a>
                                <input class=\"js-upload-file\" name=\"files[]\" type=\"file\">
                                <input class=\"uk-input uk-form-width-medium\" type=\"text\" placeholder=\"Wybierz plik\" disabled>
                            </div>
                        </div>

                    </div>
                    <div class=\"contact-form__terms\">
                        ";
        // line 177
        echo twig_get_attribute($this->env, $this->source, ($context["pages"] ?? null), "extendedField", [0 => "home", 1 => "contact_terms"], "method", false, false, false, 177);
        echo "
                    </div>
                    <div class=\"uk-width-1-1\">
                        <label><input class=\"uk-checkbox\" type=\"checkbox\" checked> ";
        // line 180
        echo "Akceptuje warunki korzystania z serwisu.";
        echo " <a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("terms");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Regulamin"]);
        echo "</a> </label>
                    </div>
                    <button type=\"submit\" class=\"contact-form__form__submit button uk-button uk-button-link uk-margin-auto\">
                        ";
        // line 183
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Wyślij"]);
        echo "</button>
                </form>
            </div>
        </div>
        <div class=\"contact-form contact-form--right\">
            <div class=\"contact-social uk-flex uk-flex-row\">
                <span class=\"contact-social__label\">";
        // line 189
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Social Media"]);
        echo ":</span>
                ";
        // line 190
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["socialmediaRecords"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["social"]) {
            // line 191
            echo "                <a href=\"";
            echo twig_escape_filter($this->env, $context["social"], "html", null, true);
            echo "\" class=\"contact-social__item\" data-uk-icon=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\"></a>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['social'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 193
        echo "            </div>
            <section class=\"contact-oh\">
                <h1 class=\"contact-oh__title\">";
        // line 195
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Godziny <br>otwarcia"]);
        echo "</h1>
                <ul class=\"contact-oh__list\">
                    ";
        // line 197
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hours"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["hour"]) {
            // line 198
            echo "                    <li class=\"contact-oh__list__item\">
                        <span class=\"contact-oh__list__item__day\">";
            // line 199
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Poniedziałek"]);
            echo "</span>
                        <span class=\"contact-oh__list__item__hours\">8.00 - 17.00</span>
                    </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hour'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 203
        echo "                </ul>
            </section>
            <div class=\"contact-data\">
                <div class=\"contact-data__title\">";
        // line 206
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Dane Firmy"]);
        echo "</div>
                <div class=\"contact-data__data uk-text-uppercase\">
                    ";
        // line 208
        echo twig_escape_filter($this->env, ($context["companyName"] ?? null), "html", null, true);
        echo "<br>
                    ";
        // line 209
        echo twig_escape_filter($this->env, ($context["address"] ?? null), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, ($context["postcode"] ?? null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["city"] ?? null), "html", null, true);
        echo "
                </div>
                <div class=\"contact-data__title\">";
        // line 211
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Telefon / Mail"]);
        echo "</div>
                <div class=\"contact-data__data\">
                    <a href=\"tel:";
        // line 213
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["phone"] ?? null), "value", [], "any", false, false, false, 213), "html", null, true);
        echo "\" class=\"contact-data__data__phone\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["phone"] ?? null), "value", [], "any", false, false, false, 213), "html", null, true);
        echo "</a><br>
                    <a href=\"mailto:";
        // line 214
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["email"] ?? null), "value", [], "any", false, false, false, 214), "html", null, true);
        echo "\" class=\"contact-data__data__email\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["email"] ?? null), "value", [], "any", false, false, false, 214), "html", null, true);
        echo "</a>
                </div>
                <div class=\"contact-data__title\">";
        // line 216
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["NIP / REGON"]);
        echo "</div>
                <div class=\"contact-data__data\">
                    NIP ";
        // line 218
        echo twig_escape_filter($this->env, ($context["taxNumber"] ?? null), "html", null, true);
        echo " / REGON: ";
        echo twig_escape_filter($this->env, ($context["registrationNumber"] ?? null), "html", null, true);
        echo "
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"uk-section uk-section-default uk-padding-remove\"  id=\"maps\">
    <div class=\"uk-width-1-1 uk-child-width-1-1 uk-child-width-1-2@m uk-flex uk-flex-wrap maps\">
        <div class=\"maps__item\">
            <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2461.474077579362!2d15.504193316107804!3d51.90706117970285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4706110c236d69bd%3A0x6caef943518c3850!2sFirma%20kamieniarska%20Grygier-Granit!5e0!3m2!1spl!2spl!4v1588259370528!5m2!1spl!2spl\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>
        </div>
        <div class=\"maps__item uk-visible@m\">
            <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2509505.3713598754!2d16.4045745014674!3d52.103592471591526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4706110c236d69bd%3A0x6caef943518c3850!2sFirma%20kamieniarska%20Grygier-Granit!5e0!3m2!1spl!2spl!4v1588773256366!5m2!1spl!2spl\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  551 => 218,  546 => 216,  539 => 214,  533 => 213,  528 => 211,  519 => 209,  515 => 208,  510 => 206,  505 => 203,  495 => 199,  492 => 198,  488 => 197,  483 => 195,  479 => 193,  468 => 191,  464 => 190,  460 => 189,  451 => 183,  441 => 180,  435 => 177,  420 => 165,  413 => 161,  409 => 160,  403 => 157,  399 => 156,  393 => 153,  389 => 152,  383 => 149,  379 => 148,  371 => 143,  365 => 139,  359 => 138,  354 => 135,  343 => 130,  336 => 127,  326 => 125,  324 => 124,  320 => 122,  316 => 121,  311 => 118,  305 => 117,  299 => 113,  288 => 108,  281 => 105,  271 => 103,  269 => 102,  265 => 100,  261 => 99,  256 => 96,  254 => 95,  248 => 94,  238 => 87,  232 => 84,  229 => 83,  223 => 82,  218 => 79,  210 => 77,  202 => 75,  200 => 74,  196 => 73,  193 => 72,  190 => 71,  186 => 70,  179 => 68,  173 => 64,  167 => 63,  165 => 62,  157 => 57,  153 => 56,  149 => 55,  143 => 52,  139 => 51,  135 => 50,  131 => 49,  125 => 46,  121 => 45,  117 => 44,  113 => 43,  104 => 39,  96 => 34,  83 => 23,  79 => 21,  77 => 20,  75 => 19,  73 => 18,  71 => 17,  69 => 16,  67 => 15,  65 => 14,  63 => 13,  61 => 12,  59 => 11,  57 => 10,  54 => 9,  52 => 8,  50 => 7,  48 => 6,  46 => 5,  44 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set page =  pages.page('home') %}
{{ seo.seo(page.seo)|raw }}

{% set postsRecords = posts.paginate(10) %}
{% set postsRecords = posts.category('realizacje', true, 20) %}
{% set iconsboxRecords = iconsbox.box('home') %}
{% set testimonialGroup = testimonial.testimonialGroup('home') %}
{% set socialmediaRecords = socialmedia.socialMedia() %}

{% set place = places.place('primary') %}
{% if place %}
{% set companyName = places.placeFiled('primary', 'name') %}
{% set phone = places.placeFiled('primary', 'phones', 'primary') %}
{% set email = places.placeFiled('primary', 'emails', 'primary') %}
{% set hours = places.placeFiled('primary', 'hours') %}
{% set address = places.placeFiled('primary', 'address') %}
{% set postcode = places.placeFiled('primary', 'post_code') %}
{% set city = places.placeFiled('primary', 'city') %}
{% set taxNumber = places.placeFiled('primary', 'tax_number') %}
{% set registrationNumber = places.placeFiled('primary', 'registration_number') %}

{% endif %}

<section class=\"uk-section uk-section-default\">
    <div class=\"container-inside uk-margin-auto uk-flex uk-grid-collapse\" uk-grid>
        <div class=\"uk-width-3-5@m uk-flex uk-flex-column uk-flex-center\">
            <div class=\"history uk-margin-auto\">
                <div class=\"history__top\">
                    <span class=\"history__top__prefix\">Od</span>
                    <span class=\"history__top__data\" data-uk-scrollspy=\"cls:uk-animation-slide-top\">44</span>
                    <span class=\"history__top__suffix\">lat.</span>
                </div>
                <div class=\"history__description\">
                    {{ pages.extendedField('home', 'about')|raw }}
                </div>
            </div>
        </div>
        <div class=\"uk-width-2-5@m uk-visible@m uk-text-center\">
            <img class=\"uk-animation-scale-up\" style=\"max-width:250px;\"src=\"{{ 'assets/dist/images/history.png'|theme }}\" data-src=\"{{ 'assets/dist/images/history.png'|theme }}\" data-width=\"\" data-height=\"\" alt=\"\" data-uk-img>
        </div>
        <div class=\"uk-width-1-1 uk-child-width-1-1 uk-child-width-1-3@m uk-flex uk-flex-wrap uk-margin-medium-top\">
            <div class=\"history-icon\">
                <span class=\"history-icon__prefix\">{{ 'Niemal'|_ }}</span>
                <span class=\"history-icon__data\" data-uk-scrollspy=\"cls:uk-animation-slide-top\">{{ 'pół'|_ }}</span>
                <span class=\"history-icon__suffix\">{{ 'wieku'|_ }}</span>
                <div class=\"history-icon__description\">{{ 'doświadczenia'|_ }}</div>
            </div>
            <div class=\"history-icon\">
                <span class=\"history-icon__prefix\">{{ 'Ponad'|_ }}</span>
                <span class=\"history-icon__data\" data-uk-scrollspy=\"cls:uk-animation-slide-top\">{{ '15'|_ }}</span>
                <span class=\"history-icon__suffix\">{{ 'tysięcy'|_ }}</span>
                <div class=\"history-icon__description\">{{ 'realizacji'|_ }}</div>
            </div>
            <div class=\"history-icon\">
                <span class=\"history-icon__prefix\">{{ 'Park'|_ }}</span>
                <span class=\"history-icon__data\" data-uk-scrollspy=\"cls:uk-animation-slide-top\">{{ 'maszyn'|_ }}</span>
                <div class=\"history-icon__description\">{{ 'najwyższej klasy'|_ }}</div>
            </div>
        </div>
    </div>
</section>
{% if postsRecords|length > 0 %}
{% partial \"components/bar\" title=\"Realizacje\" subtitle=\"Nasze\" %}
<section class=\"uk-section uk-section-default\">
    <div class=\"container-inside uk-margin-auto uk-flex uk-grid-collapse\" data-uk-grid>
        <div class=\"uk-width-1-1 uk-flex uk-flex-wrap uk-flex-wrap-reverse\">
            <div class=\"blog-list__more uk-margin\">
                <a  class=\"blog-list__more__link\" href=\"{{ 'blog/category'|page({slug: 'realizacje'}) }}\">{{ 'Zobacz więcej'|_ }}</a>
            </div>
            {% for post in postsRecords|reverse %}
            {% if post.is_home %}
            <section class=\"uk-width-1-2 uk-width-1-3@m \">
                <a href=\"{{ 'blog/post'|page({ slug: post.slug }) }}\">
                    {% if post.poster.xlarge %}
                        <img data-uk-scrollspy=\"cls:uk-animation-scale-up\" data-src=\"{{ post.poster.xlarge.thumb(400,400, 'crop') }}\" data-width=\"\" data-height=\"\" alt=\"{{ post.title }}\" data-uk-img>
                    {% else %}
                        <img data-uk-scrollspy=\"cls:uk-animation-scale-up\" data-src=\"{{ 'assets/dist/images/posts-listing--home.jpg'|theme }}\" data-width=\"\" data-height=\"\" alt=\"{{ post.title }}\" data-uk-img>
                    {% endif %}
                </a>
            </section>
            {% endif %}
            {% endfor %}
            <section class=\"uk-width-1-1 uk-width-2-3@m blog-list__header\">
                <h1>{{ pages.extendedField('home', 'realizations_title')|raw }}</h1>
                <hr>
                <div>
                    {{ pages.extendedField('home', 'realizations_description')|raw }}
                </div>
            </section>
        </div>
    </div>
</section>
{% endif %}
{% partial \"components/bar\" title=\"Otrzymasz:\" subtitle=\"Wybierając nas\" %}
{% if iconsboxRecords %}
<section class=\"uk-section uk-section-default\">
    <div class=\"container-inside uk-margin-auto uk-flex uk-grid-collapse\" data-uk-grid>
        <div class=\"uk-width-1-1 uk-child-width-1-1 uk-child-width-1-3@m uk-flex uk-flex-wrap\">
            {% for box in iconsboxRecords.icons %}
            <div class=\"uk-flex uk-flex-center\">
                <section class=\"icons-box uk-padding uk-margin-auto-left uk-margin-auto-right\">
                    {% if box.icon %}
                        <img data-uk-scrollspy=\"cls:uk-animation-scale-up\" src=\"{{ box.icon|media }}\" data-src=\"{{ box.icon|media }}\" data-width=\"\" data-height=\"\" alt=\"{{ box.name }}\" data-uk-img>
                    {% endif %}
                    <h2 class=\"icons-box__title\">{{ box.name }}</h2>
                    <hr class=\"icons-box__pipe\">
                    <p class=\"icons-box__description\">
                        {{ box.description|raw }}
                    </p>
                </section>
            </div>
            {% endfor %}
        </div>
    </div>
</section>
{% endif %}
{% partial \"components/bar\" title=\"O nas:\" subtitle=\"Kilka słów\" %}
<section class=\"uk-section uk-section-default\" id=\"about\">
    <div class=\"container-inside uk-margin-auto uk-position-relative\" data-uk-slider=\"autoplay: false; sets: true\">
        <ul class=\"uk-slider-items uk-child-width-1-1@s uk-child-width-1-3@m\">
            {% for testimonial in testimonialGroup.testimonials %}
            <li class=\"uk-flex uk-flex-center\">
                <div class=\"icons-box uk-padding\">
                    {% if testimonial.poster %}
                        <img class=\"icons-box__icon\" src=\"{{ testimonial.poster.thumb(128,128, 'crop') }}\" data-src=\"{{ testimonial.poster.thumb(128,128, 'crop') }}\" data-width=\"\" data-height=\"\" alt=\"{{ post.title }}\" data-uk-img>
                    {% endif %}
                    <h2 class=\"icons-box__title\">{{ testimonial.name }}</h2>
                    <hr class=\"icons-box__pipe\">
                    <p class=\"icons-box__description\">
                        {{ testimonial.description|raw }}
                    </p>
                </div>
            </li>
            {% endfor %}
        </ul>
    </div>
</section>
{% partial \"components/bar\" title=\"Kontaktu:\" subtitle=\"Zapraszamy do\" %}
<section class=\"uk-section uk-section-default\" id=\"contact\">
    <div class=\"container-inside uk-margin-auto uk-width-1-1 uk-child-width-1-1 uk-child-width-1-2@m uk-flex uk-flex-center uk-flex-middle uk-flex-wrap contact\">
        <div class=\"contact-form contact-form--left\">
            <div class=\"contact-form__description\">
                {{ pages.extendedField('home', 'contact_description')|raw }}
            </div>
            <div class=\"contact-form__form\">
                <form data-request=\"contact::onSubmit\" data-request-files class=\"uk-grid-small\"  data-uk-grid>
                    <div class=\"uk-width-1-2@s\">
                        <label class=\"uk-form-label\">{{ 'Imię'|_ }}</label>
                        <input class=\"uk-input\" name=\"name\" type=\"text\" placeholder=\"{{ 'Imię'|_ }}\">
                    </div>
                    <div class=\"uk-width-1-2@s\">
                        <label class=\"uk-form-label\">{{ 'Telefon'|_ }}</label>
                        <input class=\"uk-input\" name=\"phone\" type=\"tel\" placeholder=\"{{ 'Telefon'|_ }}\">
                    </div>
                    <div class=\"uk-width-1-1@s\">
                        <label class=\"uk-form-label\">{{ 'Adres email'|_ }}</label>
                        <input class=\"uk-input\" name=\"email\" type=\"email\" placeholder=\"{{ 'Adres email'|_ }}\">
                    </div>
                    <div class=\"uk-width-1-1\">
                        <label class=\"uk-form-label\">{{ 'Wiadomość'|_ }}</label>
                        <textarea class=\"uk-textarea\" rows=\"5\" name=\"message\" placeholder=\"{{ 'Wiadomość'|_ }}\"></textarea>
                    </div>
                    <div class=\"uk-width-1-1\" id=\"js-upload-container\">
                        <div>
                            <label class=\"uk-form-label\">{{ 'Załącz pliki'|_ }}</label>
                        </div>
                        <div>
                            <div class=\"uk-margin-small\" data-uk-form-custom=\"target: true\">
                                <a class=\"uk-form-icon uk-form-icon-flip\" href=\"\" data-uk-icon=\"icon: download\"></a>
                                <input class=\"js-upload-file\" name=\"files[]\" type=\"file\">
                                <input class=\"uk-input uk-form-width-medium\" type=\"text\" placeholder=\"Wybierz plik\" disabled>
                            </div>
                        </div>

                    </div>
                    <div class=\"contact-form__terms\">
                        {{ pages.extendedField('home', 'contact_terms')|raw }}
                    </div>
                    <div class=\"uk-width-1-1\">
                        <label><input class=\"uk-checkbox\" type=\"checkbox\" checked> {{ 'Akceptuje warunki korzystania z serwisu.'}} <a href=\"{{ 'terms'|page }}\">{{ 'Regulamin'|_ }}</a> </label>
                    </div>
                    <button type=\"submit\" class=\"contact-form__form__submit button uk-button uk-button-link uk-margin-auto\">
                        {{ 'Wyślij'|_ }}</button>
                </form>
            </div>
        </div>
        <div class=\"contact-form contact-form--right\">
            <div class=\"contact-social uk-flex uk-flex-row\">
                <span class=\"contact-social__label\">{{ 'Social Media'|_ }}:</span>
                {% for key, social in socialmediaRecords %}
                <a href=\"{{ social }}\" class=\"contact-social__item\" data-uk-icon=\"{{ key }}\"></a>
                {% endfor %}
            </div>
            <section class=\"contact-oh\">
                <h1 class=\"contact-oh__title\">{{ 'Godziny <br>otwarcia'|_|raw }}</h1>
                <ul class=\"contact-oh__list\">
                    {% for hour in hours %}
                    <li class=\"contact-oh__list__item\">
                        <span class=\"contact-oh__list__item__day\">{{ 'Poniedziałek'|_ }}</span>
                        <span class=\"contact-oh__list__item__hours\">8.00 - 17.00</span>
                    </li>
                    {% endfor %}
                </ul>
            </section>
            <div class=\"contact-data\">
                <div class=\"contact-data__title\">{{ 'Dane Firmy'|_ }}</div>
                <div class=\"contact-data__data uk-text-uppercase\">
                    {{ companyName }}<br>
                    {{ address }}, {{ postcode }} {{ city }}
                </div>
                <div class=\"contact-data__title\">{{ 'Telefon / Mail'|_ }}</div>
                <div class=\"contact-data__data\">
                    <a href=\"tel:{{ phone.value }}\" class=\"contact-data__data__phone\">{{ phone.value }}</a><br>
                    <a href=\"mailto:{{ email.value }}\" class=\"contact-data__data__email\">{{ email.value }}</a>
                </div>
                <div class=\"contact-data__title\">{{ 'NIP / REGON'|_ }}</div>
                <div class=\"contact-data__data\">
                    NIP {{ taxNumber }} / REGON: {{ registrationNumber }}
                </div>
            </div>
        </div>
    </div>
</section>
<section class=\"uk-section uk-section-default uk-padding-remove\"  id=\"maps\">
    <div class=\"uk-width-1-1 uk-child-width-1-1 uk-child-width-1-2@m uk-flex uk-flex-wrap maps\">
        <div class=\"maps__item\">
            <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2461.474077579362!2d15.504193316107804!3d51.90706117970285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4706110c236d69bd%3A0x6caef943518c3850!2sFirma%20kamieniarska%20Grygier-Granit!5e0!3m2!1spl!2spl!4v1588259370528!5m2!1spl!2spl\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>
        </div>
        <div class=\"maps__item uk-visible@m\">
            <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2509505.3713598754!2d16.4045745014674!3d52.103592471591526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4706110c236d69bd%3A0x6caef943518c3850!2sFirma%20kamieniarska%20Grygier-Granit!5e0!3m2!1spl!2spl!4v1588773256366!5m2!1spl!2spl\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>
        </div>
    </div>
</section>", "/var/www/html/grygier.local/themes/gg/pages/home.htm", "");
    }
}
