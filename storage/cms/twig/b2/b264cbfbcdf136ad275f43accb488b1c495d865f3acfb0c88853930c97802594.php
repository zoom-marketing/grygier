<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/partials/slider/layer.htm */
class __TwigTemplate_a8cd1061078bcdc383812f4ecfd0443d5d639136d0476c621a025626baf9cc17 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (( !(null === ($context["slide"] ?? null)) && twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_xlarge", [], "any", false, false, false, 1))) {
            // line 2
            echo "    <picture>
        ";
            // line 3
            if (twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_small", [], "any", false, false, false, 3)) {
                echo "<source media=\"(max-width: 639px)\" srcset=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_small", [], "any", false, false, false, 3), "getPath", [], "method", false, false, false, 3), "html", null, true);
                echo "\">";
            }
            // line 4
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_medium", [], "any", false, false, false, 4)) {
                echo "<source media=\"(max-width: 959px)\" srcset=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_medium", [], "any", false, false, false, 4), "getPath", [], "method", false, false, false, 4), "html", null, true);
                echo "\">";
            }
            // line 5
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_large", [], "any", false, false, false, 5)) {
                echo "<source media=\"(max-width: 1199px)\" srcset=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_large", [], "any", false, false, false, 5), "getPath", [], "method", false, false, false, 5), "html", null, true);
                echo "\">";
            }
            // line 6
            echo "        <img src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_xlarge", [], "any", false, false, false, 6), "getPath", [], "method", false, false, false, 6), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["slide"] ?? null), "layer_xlarge", [], "any", false, false, false, 6), "title", [], "any", false, false, false, 6), "html", null, true);
            echo "\">
    </picture>
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/partials/slider/layer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 6,  55 => 5,  48 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if (slide is not null) and slide.layer_xlarge %}
    <picture>
        {% if slide.layer_small %}<source media=\"(max-width: 639px)\" srcset=\"{{ slide.layer_small.getPath() }}\">{% endif %}
        {% if slide.layer_medium %}<source media=\"(max-width: 959px)\" srcset=\"{{ slide.layer_medium.getPath() }}\">{% endif %}
        {% if slide.layer_large %}<source media=\"(max-width: 1199px)\" srcset=\"{{ slide.layer_large.getPath() }}\">{% endif %}
        <img src=\"{{ slide.layer_xlarge.getPath() }}\" alt=\"{{ slide.layer_xlarge.title }}\">
    </picture>
{% endif %}", "/var/www/html/grygier.local/themes/gg/partials/slider/layer.htm", "");
    }
}
