<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/partials/layouts/default/footer.htm */
class __TwigTemplate_a8bb495a962a6c480818f3fdbb22ddab415234cfe7a3c82ea5c0eeb78c02d5e3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["obCategoryList"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["CategoryList"] ?? null), "make", [], "method", false, false, false, 1), "tree", [], "method", false, false, false, 1);
        // line 2
        $context["iconsboxCompanyRecords"] = twig_get_attribute($this->env, $this->source, ($context["iconsbox"] ?? null), "box", [0 => "company"], "method", false, false, false, 2);
        // line 3
        $context["socialmediaRecords"] = twig_get_attribute($this->env, $this->source, ($context["socialmedia"] ?? null), "socialMedia", [], "method", false, false, false, 3);
        // line 4
        $context["place"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "place", [0 => "primary"], "method", false, false, false, 4);
        // line 5
        if (($context["place"] ?? null)) {
            // line 6
            $context["phone"] = twig_get_attribute($this->env, $this->source, ($context["places"] ?? null), "placeFiled", [0 => "primary", 1 => "phones", 2 => "primary"], "method", false, false, false, 6);
        }
        // line 8
        echo "<footer class=\"footer uk-background-primary\" id=\"layout-footer\">
    <div class=\"footer__container uk-grid-collapse container-inside uk-flex-wrap-stretch \" data-uk-grid>
        <div class=\"uk-width-1-3@m footer-logo uk-visible@m\">
            <span>
                <img src=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/dist/images/logo_footer.png");
        echo "\" data-src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/dist/images/logo_footer.png");
        echo "\" data-width=\"\" data-height=\"\" alt=\"\" data-uk-img>
            </span>
        </div>
        <div class=\"uk-width-1-4@m footer-menu uk-visible@m\">
            <ul class=\"uk-list uk-text-uppercase footer-menu__list\">
                ";
        // line 17
        if (twig_get_attribute($this->env, $this->source, ($context["obCategoryList"] ?? null), "isNotEmpty", [], "method", false, false, false, 17)) {
            // line 18
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["obCategoryList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["obCategory"]) {
                if ((twig_get_attribute($this->env, $this->source, $context["obCategory"], "product_count", [], "any", false, false, false, 18) > 0)) {
                    // line 19
                    echo "                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obCategory"], "getPageUrl", [0 => "shop/category"], "method", false, false, false, 19), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obCategory"], "name", [], "any", false, false, false, 19), "html", null, true);
                    echo "</a></li>
                ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['obCategory'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "                ";
        }
        // line 22
        echo "                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("shop/promotions");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Promocje"]);
        echo "</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"";
        // line 23
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "#about\" data-uk-scroll=\"offset: 135\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["O nas"]);
        echo "</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"";
        // line 24
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/category", ["slug" => "inspiracje"]);
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Inspiracje"]);
        echo "</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"";
        // line 25
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("faq");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["FAQ"]);
        echo "</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"";
        // line 26
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("documents");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Pliki do pobrania"]);
        echo "</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"";
        // line 27
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "#contact\" data-uk-scroll=\"offset: 135\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Kontakt"]);
        echo "</a></li>
            </ul>
        </div>
        <div class=\"uk-width-expand@m uk-flex uk-flex-column uk-flex-right footer-partners\">
            <div class=\"uk-grid-collapse uk-width-expand\" data-uk-grid>
                <div class=\"uk-width-1-4@m uk-flex uk-flex-row uk-flex-column@m uk-flex-center uk-flex-middle uk-flex-left@m uk-margin-remove-bottom@m\">
                    <span>";
        // line 33
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Nasi partnerzy"]);
        echo ":</span>
                </div>
                <div class=\"uk-width-3-4@m uk-flex uk-flex-middle uk-flex-center\">
                    ";
        // line 36
        if (($context["iconsboxCompanyRecords"] ?? null)) {
            // line 37
            echo "                    <ul class=\"uk-list footer-partners__list\">
                        ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["iconsboxCompanyRecords"] ?? null), "icons", [], "any", false, false, false, 38));
            foreach ($context['_seq'] as $context["_key"] => $context["box"]) {
                // line 39
                echo "                        <li class=\"footer-partners__list__item uk-text-center\">
                            <img src=\"";
                // line 40
                echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["box"], "icon", [], "any", false, false, false, 40));
                echo "\" data-src=\"";
                echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, $context["box"], "icon", [], "any", false, false, false, 40));
                echo "\" data-width=\"\" data-height=\"\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["box"], "name", [], "any", false, false, false, 40), "html", null, true);
                echo "\" data-uk-img>
                        </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['box'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "                    </ul>
                    ";
        }
        // line 45
        echo "                </div>
            </div>
            <div class=\"uk-grid-collapse footer-partners__line\" data-uk-grid>
                <div class=\"uk-width-1-4@m uk-flex uk-flex-row uk-flex-column@m uk-flex-center uk-flex-middle uk-flex-left@m uk-margin-remove-bottom@m\">
                    <span>";
        // line 49
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Obserwuj nas"]);
        echo ":</span>
                </div>
                <div class=\"uk-width-3-4@m uk-flex uk-flex-center footer-social\">
                    ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["socialmediaRecords"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["social"]) {
            // line 53
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $context["social"], "html", null, true);
            echo "\" class=\"footer-social__item uk-icon-button uk-margin-right\" data-uk-icon=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\"></a>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['social'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                </div>
            </div>
        </div>
    </div>
</footer>
<div class=\"footer__copyright\"></div>
<div class=\"phone-sticky\">
    <a href=\"tel:";
        // line 62
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["phone"] ?? null), "value", [], "any", false, false, false, 62), "html", null, true);
        echo "\" class=\"uk-icon-button phone-sticky__icon\" data-uk-icon=\"icon: whatsapp; ratio: 2;\"></a>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/partials/layouts/default/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 62,  193 => 55,  182 => 53,  178 => 52,  172 => 49,  166 => 45,  162 => 43,  149 => 40,  146 => 39,  142 => 38,  139 => 37,  137 => 36,  131 => 33,  120 => 27,  114 => 26,  108 => 25,  102 => 24,  96 => 23,  89 => 22,  86 => 21,  74 => 19,  68 => 18,  66 => 17,  56 => 12,  50 => 8,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set obCategoryList = CategoryList.make().tree() %}
{% set iconsboxCompanyRecords = iconsbox.box('company') %}
{% set socialmediaRecords = socialmedia.socialMedia() %}
{% set place = places.place('primary') %}
{% if place %}
{% set phone = places.placeFiled('primary', 'phones', 'primary') %}
{% endif %}
<footer class=\"footer uk-background-primary\" id=\"layout-footer\">
    <div class=\"footer__container uk-grid-collapse container-inside uk-flex-wrap-stretch \" data-uk-grid>
        <div class=\"uk-width-1-3@m footer-logo uk-visible@m\">
            <span>
                <img src=\"{{ 'assets/dist/images/logo_footer.png'|theme }}\" data-src=\"{{ 'assets/dist/images/logo_footer.png'|theme }}\" data-width=\"\" data-height=\"\" alt=\"\" data-uk-img>
            </span>
        </div>
        <div class=\"uk-width-1-4@m footer-menu uk-visible@m\">
            <ul class=\"uk-list uk-text-uppercase footer-menu__list\">
                {% if obCategoryList.isNotEmpty() %}
                {% for obCategory in obCategoryList if obCategory.product_count > 0 %}
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"{{ obCategory.getPageUrl('shop/category') }}\">{{ obCategory.name }}</a></li>
                {% endfor %}
                {% endif %}
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"{{ 'shop/promotions'|page }}\">{{ 'Promocje'|_ }}</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"{{ 'home'|page }}#about\" data-uk-scroll=\"offset: 135\">{{ 'O nas'|_ }}</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"{{ 'blog/category'|page({slug: 'inspiracje'}) }}\">{{ 'Inspiracje'|_ }}</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"{{ 'faq'|page }}\">{{ 'FAQ'|_ }}</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"{{ 'documents'|page }}\">{{ 'Pliki do pobrania'|_ }}</a></li>
                <li class=\"footer-menu__list__item\"><a class=\"footer-menu__list__item__link\" href=\"{{ 'home'|page }}#contact\" data-uk-scroll=\"offset: 135\">{{ 'Kontakt'|_  }}</a></li>
            </ul>
        </div>
        <div class=\"uk-width-expand@m uk-flex uk-flex-column uk-flex-right footer-partners\">
            <div class=\"uk-grid-collapse uk-width-expand\" data-uk-grid>
                <div class=\"uk-width-1-4@m uk-flex uk-flex-row uk-flex-column@m uk-flex-center uk-flex-middle uk-flex-left@m uk-margin-remove-bottom@m\">
                    <span>{{ 'Nasi partnerzy'|_}}:</span>
                </div>
                <div class=\"uk-width-3-4@m uk-flex uk-flex-middle uk-flex-center\">
                    {% if iconsboxCompanyRecords %}
                    <ul class=\"uk-list footer-partners__list\">
                        {% for box in iconsboxCompanyRecords.icons %}
                        <li class=\"footer-partners__list__item uk-text-center\">
                            <img src=\"{{ box.icon|media }}\" data-src=\"{{ box.icon|media }}\" data-width=\"\" data-height=\"\" alt=\"{{ box.name }}\" data-uk-img>
                        </li>
                        {% endfor %}
                    </ul>
                    {% endif %}
                </div>
            </div>
            <div class=\"uk-grid-collapse footer-partners__line\" data-uk-grid>
                <div class=\"uk-width-1-4@m uk-flex uk-flex-row uk-flex-column@m uk-flex-center uk-flex-middle uk-flex-left@m uk-margin-remove-bottom@m\">
                    <span>{{ 'Obserwuj nas'|_}}:</span>
                </div>
                <div class=\"uk-width-3-4@m uk-flex uk-flex-center footer-social\">
                    {% for key, social in socialmediaRecords %}
                    <a href=\"{{ social }}\" class=\"footer-social__item uk-icon-button uk-margin-right\" data-uk-icon=\"{{ key }}\"></a>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>
</footer>
<div class=\"footer__copyright\"></div>
<div class=\"phone-sticky\">
    <a href=\"tel:{{ phone.value }}\" class=\"uk-icon-button phone-sticky__icon\" data-uk-icon=\"icon: whatsapp; ratio: 2;\"></a>
</div>", "/var/www/html/grygier.local/themes/gg/partials/layouts/default/footer.htm", "");
    }
}
