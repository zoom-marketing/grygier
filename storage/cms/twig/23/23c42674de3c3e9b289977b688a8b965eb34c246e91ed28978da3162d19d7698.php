<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/layouts/default.htm */
class __TwigTemplate_1eac87530379f1601071516df3f371e6797b20d9c54ee5ae5449c20c0134f6e3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("aditionalcode"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 5
        $context['__placeholder_seo_default_contents'] = null;        ob_start();        // line 6
        echo "            <title>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 6), "title", [], "any", false, false, false, 6), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 6), "site_name", [], "any", false, false, false, 6), "html", null, true);
        echo "</title>
            <meta name=\"description\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 7), "meta_description", [], "any", false, false, false, 7), "html", null, true);
        echo "\">
            <meta name=\"title\" content=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, false, 8), "meta_title", [], "any", false, false, false, 8), "html", null, true);
        echo "\">
            <meta name=\"author\" content=\"Zoom-Marketing.pl\">
        ";
        $context['__placeholder_seo_default_contents'] = ob_get_clean();        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('seo', $context['__placeholder_seo_default_contents']);
        unset($context['__placeholder_seo_default_contents']);        // line 11
        echo "        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        ";
        // line 13
        $context['__placeholder_appIcons_default_contents'] = null;        ob_start();        // line 14
        echo "        <link rel=\"icon\" type=\"image/png\" href=\"";
        echo call_user_func_array($this->env->getFunction('mix')->getCallable(), ["/images/favicon.png"]);
        echo "\">
        ";
        $context['__placeholder_appIcons_default_contents'] = ob_get_clean();        // line 13
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('appIcons', $context['__placeholder_appIcons_default_contents']);
        unset($context['__placeholder_appIcons_default_contents']);        // line 16
        echo "        <link href=\"";
        echo call_user_func_array($this->env->getFunction('mix')->getCallable(), ["/css/app.css"]);
        echo "\" rel=\"stylesheet\">
        ";
        // line 17
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('css');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('styles');
        // line 18
        echo "        ";
        $context['__placeholder_headAfter_default_contents'] = null;        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('headAfter', $context['__placeholder_headAfter_default_contents']);
        unset($context['__placeholder_headAfter_default_contents']);        // line 19
        echo "    </head>
    <body>
    ";
        // line 21
        $context['__placeholder_bodyBefore_default_contents'] = null;        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('bodyBefore', $context['__placeholder_bodyBefore_default_contents']);
        unset($context['__placeholder_bodyBefore_default_contents']);        // line 22
        echo "        <main class=\"main\" id=\"app\">
            ";
        // line 23
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("layouts/default/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 24
        echo "            ";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 25
        echo "            ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("layouts/default/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 26
        echo "        </main>
        <script src=\"";
        // line 27
        echo call_user_func_array($this->env->getFunction('mix')->getCallable(), ["/js/app.js"]);
        echo "\" defer></script>
        <script type=\"text/javascript\" src=\"";
        // line 28
        echo $this->extensions['Cms\Twig\Extension']->themeFilter([0 => "@framework", 1 => "@framework.extras"]);
        // line 31
        echo ".js\" defer>
        </script>
        ";
        // line 33
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 34
        echo "    ";
        $context['__placeholder_bodyAfter_default_contents'] = null;        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('bodyAfter', $context['__placeholder_bodyAfter_default_contents']);
        unset($context['__placeholder_bodyAfter_default_contents']);        // line 35
        echo "    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 35,  125 => 34,  122 => 33,  118 => 31,  116 => 28,  112 => 27,  109 => 26,  104 => 25,  101 => 24,  97 => 23,  94 => 22,  92 => 21,  88 => 19,  85 => 18,  82 => 17,  77 => 16,  75 => 13,  70 => 14,  69 => 13,  65 => 11,  63 => 5,  58 => 8,  54 => 7,  47 => 6,  46 => 5,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% component 'aditionalcode' %}
<!DOCTYPE html>
<html>
    <head>
        {% placeholder seo default %}
            <title>{{ this.page.title }} - {{ this.theme.site_name }}</title>
            <meta name=\"description\" content=\"{{ this.page.meta_description }}\">
            <meta name=\"title\" content=\"{{ this.page.meta_title }}\">
            <meta name=\"author\" content=\"Zoom-Marketing.pl\">
        {% endplaceholder %}
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        {% placeholder appIcons default %}
        <link rel=\"icon\" type=\"image/png\" href=\"{{ mix('/images/favicon.png') }}\">
        {% endplaceholder %}
        <link href=\"{{ mix('/css/app.css') }}\" rel=\"stylesheet\">
        {% styles %}
        {% placeholder headAfter %}
    </head>
    <body>
    {% placeholder bodyBefore %}
        <main class=\"main\" id=\"app\">
            {% partial 'layouts/default/header' %}
            {% page %}
            {% partial 'layouts/default/footer' %}
        </main>
        <script src=\"{{ mix('/js/app.js') }}\" defer></script>
        <script type=\"text/javascript\" src=\"{{ [
            '@framework',
            '@framework.extras'
        ]|theme }}.js\" defer>
        </script>
        {% scripts %}
    {% placeholder bodyAfter %}
    </body>
</html>", "/var/www/html/grygier.local/themes/gg/layouts/default.htm", "");
    }
}
