<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/pages/shop/promotions.htm */
class __TwigTemplate_e321adedb98d02cbc56e0f0b834e4aeaa882740746e26f833113f81e13f9e05c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["page"] = twig_get_attribute($this->env, $this->source, ($context["pages"] ?? null), "page", [0 => "shop-promotions"], "method", false, false, false, 1);
        // line 2
        echo twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "seo", [0 => twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "seo", [], "any", false, false, false, 2)], "method", false, false, false, 2);
        echo "
";
        // line 4
        $context["obPromoBlockList"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["PromoBlockList"] ?? null), "make", [], "method", false, false, false, 4), "active", [], "method", false, false, false, 4);
        // line 6
        $context["arPromoBlockList"] = twig_get_attribute($this->env, $this->source, ($context["obPromoBlockList"] ?? null), "random", [0 => 5], "method", false, false, false, 6);
        // line 7
        echo "

";
        // line 9
        if ( !twig_test_empty(($context["arPromoBlockList"] ?? null))) {
            // line 10
            echo "
";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["arPromoBlockList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["obPromoBlock"]) {
                // line 13
                echo "<section class=\"uk-section uk-section-default uk-padding\">
    <div class=\"container-inside container-inside--s uk-margin-auto uk-width-1-1\">

            <div>
                ";
                // line 17
                if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "preview_image", [], "any", false, false, false, 17))) {
                    // line 18
                    echo "                <img
                    src=\"";
                    // line 19
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "preview_image", [], "any", false, false, false, 19), "path", [], "any", false, false, false, 19), "html", null, true);
                    echo "\"
                    itemprop=\"image\"
                    alt=\"";
                    // line 21
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "preview_image", [], "any", false, false, false, 21), "description", [], "any", false, false, false, 21), "html", null, true);
                    echo "\"
                    title=\"";
                    // line 22
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "preview_image", [], "any", false, false, false, 22), "title", [], "any", false, false, false, 22), "html", null, true);
                    echo "\">
                ";
                }
                // line 24
                echo "                <h3 itemprop=\"name\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "name", [], "any", false, false, false, 24), "html", null, true);
                echo "</h3>
                ";
                // line 25
                if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "preview_text", [], "any", false, false, false, 25))) {
                    // line 26
                    echo "                <div itemprop=\"description\">
                    ";
                    // line 27
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "preview_text", [], "any", false, false, false, 27), "html", null, true);
                    echo "
                </div>
                ";
                }
                // line 30
                echo "            </div>
    </div>
</section>

";
                // line 34
                if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "product", [], "any", false, false, false, 34)) > 0)) {
                    // line 35
                    $context["arProductList"] = twig_get_attribute($this->env, $this->source, $context["obPromoBlock"], "product", [], "any", false, false, false, 35);
                    // line 36
                    echo "<section class=\"uk-section uk-section-default\">
    <div class=\"uk-container uk-container-expand uk-padding-remove\">
        <ul class=\"products-list uk-flex uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-6@m uk-flex-wrap\">
            ";
                    // line 39
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["arProductList"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["obProduct"]) {
                        // line 40
                        echo "            <li class=\"products-list__item\">
                ";
                        // line 41
                        $context['__cms_partial_params'] = [];
                        $context['__cms_partial_params']['obProduct'] = $context["obProduct"]                        ;
                        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("shop/product-listing"                        , $context['__cms_partial_params']                        , true                        );
                        unset($context['__cms_partial_params']);
                        // line 42
                        echo "            </li>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['obProduct'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 44
                    echo "        </ul>
    </div>
</section>
";
                }
                // line 48
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['obPromoBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/pages/shop/promotions.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 48,  135 => 44,  128 => 42,  123 => 41,  120 => 40,  116 => 39,  111 => 36,  109 => 35,  107 => 34,  101 => 30,  95 => 27,  92 => 26,  90 => 25,  85 => 24,  80 => 22,  76 => 21,  71 => 19,  68 => 18,  66 => 17,  60 => 13,  56 => 12,  53 => 10,  51 => 9,  47 => 7,  45 => 6,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set page =  pages.page('shop-promotions') %}
{{ seo.seo(page.seo)|raw }}
{# Get promo block collection #}
{% set obPromoBlockList = PromoBlockList.make().active() %}
{# Get array with random promo blocks #}
{% set arPromoBlockList = obPromoBlockList.random(5) %}


{% if arPromoBlockList is not empty %}

{# Render promo block list #}
{% for obPromoBlock in arPromoBlockList %}
<section class=\"uk-section uk-section-default uk-padding\">
    <div class=\"container-inside container-inside--s uk-margin-auto uk-width-1-1\">

            <div>
                {% if obPromoBlock.preview_image is not empty %}
                <img
                    src=\"{{ obPromoBlock.preview_image.path }}\"
                    itemprop=\"image\"
                    alt=\"{{ obPromoBlock.preview_image.description }}\"
                    title=\"{{ obPromoBlock.preview_image.title }}\">
                {% endif %}
                <h3 itemprop=\"name\">{{ obPromoBlock.name }}</h3>
                {% if obPromoBlock.preview_text is not empty %}
                <div itemprop=\"description\">
                    {{ obPromoBlock.preview_text }}
                </div>
                {% endif %}
            </div>
    </div>
</section>

{% if obPromoBlock.product|length > 0 %}
{% set arProductList = obPromoBlock.product %}
<section class=\"uk-section uk-section-default\">
    <div class=\"uk-container uk-container-expand uk-padding-remove\">
        <ul class=\"products-list uk-flex uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-6@m uk-flex-wrap\">
            {% for obProduct in arProductList %}
            <li class=\"products-list__item\">
                {% partial 'shop/product-listing' obProduct=obProduct %}
            </li>
            {% endfor %}
        </ul>
    </div>
</section>
{% endif %}

{% endfor %}
{% endif %}", "/var/www/html/grygier.local/themes/gg/pages/shop/promotions.htm", "");
    }
}
