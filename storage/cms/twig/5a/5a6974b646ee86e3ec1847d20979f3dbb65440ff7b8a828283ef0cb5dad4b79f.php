<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/partials/components/bar.htm */
class __TwigTemplate_f5a2bacdce2be3582bc0d9178d2f6e506ef3f89f6b70be822eee302b5290eeed extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"uk-section uk-section-default bar\" uk-img>
    <div class=\"container-inside uk-margin-auto\">
        <p class=\"bar__subtitle\">";
        // line 3
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [($context["subtitle"] ?? null)]);
        echo "</p>
        <h2 class=\"bar__title\">";
        // line 4
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), [($context["title"] ?? null)]);
        echo "</h2>
    </div>
    <hr class=\"bar__pipe\"/>
</section>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/partials/components/bar.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section class=\"uk-section uk-section-default bar\" uk-img>
    <div class=\"container-inside uk-margin-auto\">
        <p class=\"bar__subtitle\">{{ subtitle|_ }}</p>
        <h2 class=\"bar__title\">{{ title|_ }}</h2>
    </div>
    <hr class=\"bar__pipe\"/>
</section>", "/var/www/html/grygier.local/themes/gg/partials/components/bar.htm", "");
    }
}
