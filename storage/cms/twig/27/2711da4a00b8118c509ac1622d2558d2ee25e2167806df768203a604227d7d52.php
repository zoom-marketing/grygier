<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/plugins/grape/seo/components/seo/default.htm */
class __TwigTemplate_e6980168f4e337f482230ae2428903bf99c3d30b88caf9266ce07d435f9e8ade extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- SEO::Start -->
";
        // line 2
        if (( !(null === ($context["seo"] ?? null)) && twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "is_active", [], "any", false, false, false, 2))) {
            // line 3
            echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('seo'            );
            // line 4
            echo "    <title>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "title", [], "any", false, false, false, 4), "html", null, true);
            echo "</title>
    ";
            // line 5
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "description", [], "any", false, false, false, 5))) {
                echo "<meta name=\"description\" content=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "description", [], "any", false, false, false, 5), "html", null, true);
                echo "\" />";
            }
            // line 6
            echo "    ";
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "title", [], "any", false, false, false, 6))) {
                echo "<meta name=\"title\" content=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "title", [], "any", false, false, false, 6), "html", null, true);
                echo "\" />";
            }
            // line 7
            echo "    ";
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "keywords", [], "any", false, false, false, 7))) {
                echo "<meta name=\"keywords\" content=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "keywords", [], "any", false, false, false, 7), "html", null, true);
                echo "\" />";
            }
            // line 8
            echo "    ";
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "robots", [], "any", false, false, false, 8))) {
                echo "<meta name=\"robots\" content=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "robots", [], "any", false, false, false, 8), "html", null, true);
                echo "\" />";
            }
            // line 9
            echo "    ";
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "canonical", [], "any", false, false, false, 9))) {
                echo "<link rel=\"canonical\" href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "canonical", [], "any", false, false, false, 9), "html", null, true);
                echo "\" />";
            }
            // line 10
            echo "    <meta name=\"author\" content=\"Zoom-Marketing.pl\" />
    ";
            // line 11
            echo twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "additional_code", [], "any", false, false, false, 11);
            echo "
";
            // line 3
            echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true            );
        }
        // line 14
        echo "<!-- SEO::End -->
";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/plugins/grape/seo/components/seo/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 14,  90 => 3,  86 => 11,  83 => 10,  76 => 9,  69 => 8,  62 => 7,  55 => 6,  49 => 5,  44 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- SEO::Start -->
{% if (seo is not null) and seo.is_active  %}
{% put seo %}
    <title>{{ seo.title }}</title>
    {% if seo.description is not empty %}<meta name=\"description\" content=\"{{ seo.description }}\" />{% endif %}
    {% if seo.title is not empty %}<meta name=\"title\" content=\"{{ seo.title }}\" />{% endif %}
    {% if seo.keywords is not empty %}<meta name=\"keywords\" content=\"{{ seo.keywords }}\" />{% endif %}
    {% if seo.robots is not empty %}<meta name=\"robots\" content=\"{{ seo.robots }}\" />{% endif %}
    {% if seo.canonical is not empty %}<link rel=\"canonical\" href=\"{{ seo.canonical }}\" />{% endif %}
    <meta name=\"author\" content=\"Zoom-Marketing.pl\" />
    {{ seo.additional_code|raw }}
{% endput %}
{% endif %}
<!-- SEO::End -->
", "/var/www/html/grygier.local/plugins/grape/seo/components/seo/default.htm", "");
    }
}
