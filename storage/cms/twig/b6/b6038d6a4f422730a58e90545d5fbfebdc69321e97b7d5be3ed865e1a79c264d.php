<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/grygier.local/themes/gg/partials/layouts/default/header-page.htm */
class __TwigTemplate_56a746bbae6dcaa4752237ccc261d159117634f4e431599953d43b0467fcdf77 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header class=\"header\" id=\"layout-header\">
    <div class=\"header-navbar\" uk-sticky>
        ";
        // line 3
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['mode'] = "dark"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("components/navbar-contact"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 4
        echo "        ";
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['mode'] = "dark"        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("components/navbar-menu"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 5
        echo "    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/grygier.local/themes/gg/partials/layouts/default/header-page.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 5,  46 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<header class=\"header\" id=\"layout-header\">
    <div class=\"header-navbar\" uk-sticky>
        {% partial 'components/navbar-contact' mode=\"dark\" %}
        {% partial 'components/navbar-menu' mode=\"dark\" %}
    </div>
</header>", "/var/www/html/grygier.local/themes/gg/partials/layouts/default/header-page.htm", "");
    }
}
